package message;

import java.io.Serializable;
import java.util.ArrayList;


public class TestMessage extends EmptyChatMessage implements Serializable{
	

	private static final long serialVersionUID = 1L;
	private String username;
	private boolean adding;
	
	public TestMessage(String user, boolean addOrNot)
	{
		username = user;
		adding = addOrNot;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public boolean isAdding() {
		return adding;
	}

	public void setAdding(boolean adding) {
		this.adding = adding;
	}
	
    

}
