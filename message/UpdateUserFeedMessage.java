package message;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Vector;

import client.Account;

// receives the post information from the server

public class UpdateUserFeedMessage implements Serializable {

	public static final long serialVersionUID = 1L;
	
	//private String username;
	private int amountOfPosts; //to return
	private Vector<Account> accounts;
	private Vector<String> messages;
	private Vector<String> materials;
	private Vector<Integer> amounts;
	
	public UpdateUserFeedMessage(Vector<Account> usernames, int amountOfPosts) {
		this.accounts = usernames;
		this.amountOfPosts = amountOfPosts;
	}
	
	public UpdateUserFeedMessage(Vector<Account> usernames, int amountOfPosts, Vector<String> messages,
			Vector<String> materials, Vector<Integer> amounts) {
		this(usernames, amountOfPosts);
		this.messages = messages;
		this.materials = materials;
		this.amounts = amounts;
	}
	
	/** returns the amount of most recent posts to return PER USERNAME */
	public int getAmountOfPosts() { return amountOfPosts; }
	
	/*
	 * for UserFeed. returns a new array that connects all the separate information into one.
	 * {0username, 1message, 2material, 3amount}
	 */
	public String[] getPost(int i) {
		return new String[] {
			accounts.get(i).getUsername(),
			messages.get(i),
			materials.get(i),
			Integer.toString(amounts.get(i))
		};
	}
	
	public Vector<Account> getAccounts() { return accounts; }
	public Vector<String> getMessages() { return messages; }
	public Vector<String> getMaterials() { return materials; }
	public Vector<Integer> getAmounts() { return amounts; }
	
	public Account getAccount(int i) {
		if ( accounts != null && (i >= 0 && i < accounts.size()) ) return accounts.get(i);
		return null;
	}
	public String getUsername(int i) {
		if ( accounts != null && (i >= 0 && i < accounts.size()) ) return accounts.get(i).getUsername();
		return null;
	}
	public String getMessage(int i) {
		if ( messages != null && (i >= 0 && i < messages.size()) ) return messages.get(i);
		return null;
	}
	public String getMaterial(int i) {
		if ( i < materials.size())
			return materials.get(i);
		return null;
	}
	public Integer getAmount(int i) {
		if (i < amounts.size())
			return amounts.get(i);
		else return null;
	}
	
	public void setAccounts(Vector<Account> usernames) { this.accounts = usernames; }
	public void setMessages(Vector<String> messages) { this.messages = messages; }
	public void setMaterials(Vector<String> materials) { this.materials = materials; }
	public void setAmounts(Vector<Integer> amounts) { this.amounts = amounts; }
	
	/*
	public void setAccount(int index, Account username) { accounts.set(index, username); }
	public void setMessage(int index, String message) { messages.set(index, message); }
	public void setMaterial(int index, String material) { materials.set(index, material); }
	public void setAmount(int index, Integer amount) { amounts.set(index, amount); }
	public void setAmount(int index, int amount) { amounts.set(index, new Integer(amount)); }
	*/
}
