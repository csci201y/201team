package message;

import java.io.Serializable;
import java.util.ArrayList;


public class StartChatMessage extends EmptyChatMessage implements Serializable{
	
	//public static final long serialVersionUID = 1L;
	private static final long serialVersionUID = 1L;
	
	 
	private String sendername;
	private ArrayList<String> Allname;
	private ArrayList<String> RequestChatname;
    public StartChatMessage(String sendername, ArrayList<String> Allname, ArrayList<String> RequestChatname){
    	 
    	this.sendername = sendername;
    	this.Allname = Allname;
    	this.RequestChatname = RequestChatname;
    }
    
    public String getSender(){
    	return sendername;
    }
    
    public ArrayList <String> getAllChatname(){
    	return Allname;
    }
    
    public ArrayList <String> getRequestChatname(){
    	return RequestChatname;
    }    
    
    public void setAllChatname(ArrayList<String> t){
    	Allname = t;
    }
    
    public void setHasChatname(ArrayList<String> t){
    	RequestChatname = t;
    }
    

}
