package message;

import java.io.Serializable;


//writes the post information to the server

public class UpdateFeedMessage implements Serializable {

	private static final long serialVersionUID = 1L;
	private String username;
	private String message;
	private String material;
	private int amount;
	
	public UpdateFeedMessage(String username) {
		this.username = username;
	}
	
	public UpdateFeedMessage(String username, String messageOfMostRecentAsk,
									   String material, int amount)
	{
		this(username);
		message = messageOfMostRecentAsk;
		this.amount = amount;
		this.material = material;
	}
	
	public String getUsername() { return username; }
	public String getMessage() { return message; }
	public String getMaterial() { return material; }
	public int getAmount()		{ return amount; }
	
	public void setMostRecentAsk(String message, String material, int amount) {
		this.message = message;
		this.material = material;
		this.amount = amount;
	}
	public void setMaterial(String material) { this.material = material; }
	public void setMessage(String message) { this.message = message; }
	public void setAmount(int amount) { this.amount = amount; }
}
