package message;

import java.io.Serializable;

public class SenderMessage extends EmptyChatMessage implements Serializable{

	public static final long serialVersionUID = 1L;
	 
	 private String Sender;
	 private String Receiver;
	 public SenderMessage(String Sender, String Receiver){
		 this.Sender = Sender;
		 this.Receiver = Receiver;
	 }
	 
	 public String getSender(){
		 return Sender;
	 }
	 
	 public String getRceiver(){
		 return Receiver;
	 }
	 
	 
	 public void setSender(String m){
		 Sender = m;
	 }
	 
	 public void setRceiver(String m){
		 Receiver = m;
	 }
	 
}
