package message;

import java.io.Serializable;

public class UpdateFavoritesMessage implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String username;
	private boolean adding;
	
	public UpdateFavoritesMessage(String user, boolean addOrNot)
	{
		username = user;
		adding = addOrNot;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public boolean isAdding() {
		return adding;
	}

	public void setAdding(boolean adding) {
		this.adding = adding;
	}
	
	//returns whether the account is to be added to favorites or not (if not, then it's to be deleted


}
