package message;

import java.io.Serializable;
import java.util.Vector;

import client.Account;

public class FavoriteFeedMessage implements Serializable {
	private String name;
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setFavorites(Vector<Account> favorites) {
		this.favorites = favorites;
	}

	private Vector<Account> favorites;
	public static final long serialVersionUID = 1;
	
	public FavoriteFeedMessage(String name,Vector<Account> favs)
	{
		this.name = name;
		favorites = favs;
	}
	
	public Vector<Account> getFavorites()
	{
		return favorites;
	}

}
