package message;

import java.io.Serializable;

import client.Account;

public class SearchMessage implements Serializable {
	private static final long serialVersionUID = 1L;
	private String username;
	private Account account;
	
	public SearchMessage(String username) {
		this.username = username;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public Account getAccount() {
		return account;
	}
	public void setAccount(Account account) {
		this.account = account;
	}

}
