package message;

import java.io.Serializable;

import client.Account;

public class ValidateMessage implements Serializable {

	private static final long serialVersionUID = 1L;
	
	String signupSignal;
	Account account;
	
	public Account getAccount() {
		return account;
	}
	public void setAccount(Account account) {
		this.account = account;
	}
	public ValidateMessage(String exists, Account account){
		this.signupSignal = exists;
		this.account = account;
	}
	public ValidateMessage(String exists){
		this.signupSignal = exists;
		//this.account = account;
	}
	public String isExists() {
		return signupSignal;
	}
	public void setExists(String signupSignal) {
		this.signupSignal = signupSignal;
	}
	
	
}
