package message;

import java.io.Serializable;
import java.util.Vector;

import client.Account;

public class DefaultFeedMessage implements Serializable {
	
	public static final long serialVersionUID = 1;
	private Vector<Account> accounts;
	public void setAccounts(Vector<Account> accounts) {
		this.accounts = accounts;
	}

	private boolean fromClient; //true if the message was sent by client, false if it was sent by server
	
	public DefaultFeedMessage(Vector<Account> defaultAccounts, boolean sending)
	{
		accounts = defaultAccounts;
		fromClient = sending;
	}
	
	public Vector<Account> getAccounts()
	{
		return accounts;
	}
	
	//true if the message was sent from client, false if it was sent by server
	public boolean wasFromClient()
	{
		return fromClient;
	}

}
