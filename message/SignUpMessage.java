package message;

import java.io.Serializable;

import client.Account;

public class SignUpMessage implements Serializable {

	private static final long serialVersionUID = 1L;
	private String username;
	private String password;
	private String accountType;
	private int profilePicture;
	private Account account;
	private String email;
	private String description;
	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
		account.setContact(email);
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
		account.setDescription(description);
	}

	public SignUpMessage(String username, String password, String accountType, int profilePicture) {
		this.username = username;
		this.password = password;
		this.accountType = accountType;
		this.profilePicture = profilePicture;
		account = new Account(username, password, accountType, profilePicture);
	}
	
	public String getUsername() {
		return username;
	}

	public String getPassword() {
		return password;
	}
	
	public String getAccountType() {
		return accountType;
	}
	
	public int getProfilePicture() {
		return profilePicture;
	}
	
	public Account getAccount() {
		return account;
	}
	
}
