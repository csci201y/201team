package message;
import java.io.Serializable;
import java.net.InetAddress;
public class ChatMessage extends EmptyChatMessage implements Serializable{ //contain all the information for 
	//sender receiver 
	 
	    public static final long serialVersionUID = 1L;
		
		private String sendname;
		private String receivename;
		private String message;
		
		public ChatMessage(String sendname, String receivename, String message) {
			this.sendname = sendname;
			this.receivename = receivename;
			this.message = message;
		}
		
		public String getsendName() {
			return sendname;
		}
		

		public String getreceiveName() {
			return receivename;
		}
		
		public String getMessage() {
			return message;
		}
		
		 
		public void setSendname(String s){
			sendname = s;
		}
		
		public void setReceivename(String s){
			receivename = s;
		}
		
		public void setMessage(String s){
			message = s;
		}
		
		
}

