package message;

import java.io.Serializable;

public class MakeRequestMessage implements Serializable {

	public static final long serialVersionUID = 1L;
	
	private String username, message, material;
	private int amount;
	
	public MakeRequestMessage(String username, String message, String material, int amount) {
		this.username = username;
		this.message = message;
		this.material = material;
		this.amount = amount;
	}
	
	public String getUsername() { return username; }
	public String getMessage() 	{ return message; }
	public String getMaterial() { return material; }
	public int getAmount() 		{ return amount; }
	
	public void setUsername(String username)	{ this.username = username; }
	public void setMessage(String message) 		{ this.message = message; }
	public void setMaterial(String material) 	{ this.material = material; }
	public void setAmount(int amount) 			{ this.amount = amount; }
	
}
