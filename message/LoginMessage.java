package message;

import java.io.Serializable;

public class LoginMessage implements Serializable {
	private static final long serialVersionUID = 1;
	
	private String username;
	private String password;
	
	public LoginMessage(String username, String password) {
		this.username = username;
		this.password = password;
	}
	
	public String getUsername() {
		return username;
	}
	
	public String getPassword() {
		return password;
	}

	public String getPasscode() {
		// TODO Auto-generated method stub
		return null;
	}

}