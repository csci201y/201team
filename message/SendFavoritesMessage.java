package message;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Vector;

import client.Account;

public class SendFavoritesMessage implements Serializable {

	public static final long serialVersionUID = 1;
	private Vector<Account> favorites;
	
	public SendFavoritesMessage(Vector<Account> favs)
	{
		favorites = favs;
	}
	
	public Vector<Account>  getFavorites()
	{
		return favorites;
	}

	public void setFavorites(Vector<Account>  favorites) {
		this.favorites = favorites;
	}
	
}
