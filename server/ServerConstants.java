package server;

public class ServerConstants {
	public static final String SIGNUPFAILURE_EXISTS = "account already exists failure";
	public static final String SIGNUPSUCCESS = "success";

	public static final String LOGINFAILURE = "account log-in failure";
	public static final String LOGINFAILURE_NOTEXIST = "account not exists failure";
	public static final String LOGINSUCCESS = "log-in success";
	//public static final String LOGINFAILURE_PASSWORD = "wrong password";
	public static final int numDefaultAccounts = 6;
	
	public static final String ADD_FAV_FAILURE_ACCOUNT_NOTEXISTS = "account does not exists";
	public static final String ADD_FAV_FAILURE_IN_FAV = "account already in favorite lists";
	public static final String ADD_FAV_SUCCESS = "account successfully added";
	public static final String REMOVE_FAV_FAILURE_ACCOUNT_NOTEXISTS = "account does not exists";
	public static final String REMOVE_FAV_FAILURE_NOT_IN_FAV = "account already not favorite lists";
	public static final String REMOVE_FAV_SUCCESS = "favorite successfully removed";
}
