package server;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.BindException;
import java.net.ServerSocket;
import java.util.StringTokenizer;

public class ProgramServer {
		private ServerSocket socket;
		private  CollectionThread thread; // wait for connection
		private ProgramServerGUI serverGUI;
		private SQLDriver sqlDriver;
		private int port;
		
		public ProgramServer() {
			parseServer();
			initializeVariable();
		}
		
		public SQLDriver getSqlDriver() {
			return sqlDriver;
		}
		
		private void initializeVariable(){
			serverGUI = new ProgramServerGUI(this);
			sqlDriver = new SQLDriver();
		}
		
		public void updataText(String s){
			//serverGUI.addMessage(s);
		}
		
		public int getPort() {
			return port;
		}

		public void setPort(int port) {
			this.port = port;
		}

		public void parseServer() {
			try {
				FileReader ifr = new FileReader(new File("src/resources/config/Server.config"));
				BufferedReader br = new BufferedReader(ifr);
				String line = br.readLine();
				StringTokenizer st = new StringTokenizer(line); // split by space
				String temp = st.nextToken();
				port = Integer.parseInt(st.nextToken());
				//System.out.println(port);

			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		public void serverConnection() {
			try {
				socket = new ServerSocket(port);
			}catch(BindException e){ 
			
			}catch (IOException e) {
				e.printStackTrace();
			}
			
			thread = new CollectionThread(socket, this);
			// System.out.println("Server Connect");
			thread.start();
		}

		public void closeServer() {
			thread.closeServerThread();
		}

		public ServerSocket getSocket() {
			return socket;
		}

		public void setSocket(ServerSocket socket) {
			this.socket = socket;
		}

		public static void main(String[] args) {
			new ProgramServer();
		}

}
