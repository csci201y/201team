package server;

import java.io.BufferedReader;
import java.io.EOFException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.SocketException;
import java.util.ArrayList;

import client.Account;
import client.FavoriteFeedbackMessage;
import message.AcceptMessage;
import message.ChatMessage;
import message.CleanMessage;
import message.DefaultFeedMessage;
import message.FavoriteFeedMessage;
import message.LoginMessage;
import message.SearchMessage;
import message.SendFavoritesMessage;
import message.SenderMessage;
import message.SignUpMessage;
import message.StartChatMessage;
import message.TestMessage;
import message.UpdateFavoritesMessage;
import message.UpdateFeedMessage;
import message.UpdateUserFeedMessage;
import message.ValidateMessage;

public class Communicators extends Thread{
	private Socket socket;
	private ObjectOutputStream oos;
	private ObjectInputStream ois;
	private BufferedReader br;
	private CollectionThread collectionThread;
	private ProgramServer programServer;
	private String myName;
	//private String comName;
	Communicators(Socket socket, CollectionThread collectionThread, ProgramServer os) {
		this.programServer = os;
		this.socket = socket;
		this.collectionThread= collectionThread;
		myName = null;
		try {
			this.oos = new ObjectOutputStream(socket.getOutputStream());
			this.ois = new ObjectInputStream(socket.getInputStream());
			this.start();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	}

	public ObjectOutputStream getOos() {
		return oos;
	}

	public void setOos(ObjectOutputStream oos) {
		this.oos = oos;
	}

	public ObjectInputStream getOis() {
		return ois;
	}

	public void setOis(ObjectInputStream ois) {
		this.ois = ois;
	}

	public Socket getSocket() {
		return socket;
	}

	public void setSocket(Socket socket) {
		this.socket = socket;
	}
	
	public void run(){
		try {
			while(true){
				Object receiveMessage = ois.readObject();
				System.out.println((receiveMessage instanceof UpdateFavoritesMessage) + " server received message!");
				if(receiveMessage instanceof SignUpMessage){//Sign Up
					SignUpMessage lm = (SignUpMessage)receiveMessage;
					String username = lm.getUsername();
					String password = lm.getPassword();
					//System.out.println(password);
					String s1 = "Sign-up attemp User:" + username + " Pass:" + password;
					programServer.updataText(s1);
					boolean exists = programServer.getSqlDriver().doesExist(username);
					if (exists) {// failed
						String temp = "Sign-up failure " + "User:" + username;
						programServer.updataText(temp);
						//Account account = new Account(username, password);
						oos.writeObject(new ValidateMessage(ServerConstants.SIGNUPFAILURE_EXISTS));
						oos.flush();
						
					}else{//success
						//System.out.println("Is it a null" +lm.getEmail() + lm.getDescription());
						String temp = "Sign-up success " + "User:" + lm.getUsername();
						programServer.updataText(temp);
						programServer.getSqlDriver().addAccount(username, password,lm.getAccountType(), lm.getProfilePicture(),lm.getEmail(),lm.getDescription());
						Account account = ((SignUpMessage) receiveMessage).getAccount();
						//System.out.println("Server Account sent: " + account.getDescription());
						//System.out.println("we get account " + account.getProfilePicture());
						oos.writeObject(new ValidateMessage(ServerConstants.SIGNUPSUCCESS, account));
						oos.flush();
						myName = username;
					}
				}else if(receiveMessage instanceof LoginMessage){
					LoginMessage lm = (LoginMessage)receiveMessage;
					String username = lm.getUsername();
					String password = lm.getPassword();
					String s1 = "Log-in attemp User:" + username + " Pass:" + password;
					programServer.updataText(s1);
					boolean exists = programServer.getSqlDriver().doesExist(username);
					if (exists) {// success
						boolean correct = programServer.getSqlDriver().password(username, password);
						
						if(correct){//password correct
							String temp = "Log-in success " + "User:" + username;
							programServer.updataText(temp);
							//Account account = ((LoginMessage) receiveMessage).getAccount();
							Account account = programServer.getSqlDriver().getAccount(username);
							oos.writeObject(new ValidateMessage(ServerConstants.LOGINSUCCESS, account));
							oos.flush();
							myName = username;
						}else{//fail
							String temp = "Log-in failure " + "User:" + lm.getUsername();
							programServer.updataText(temp);
							oos.writeObject(new ValidateMessage(ServerConstants.LOGINFAILURE));
							oos.flush();
						}
					}else{//if the user name does not exists, fail
						String temp = "Log-in failure " + "User:" + lm.getUsername();
						programServer.updataText(temp);
						oos.writeObject(new ValidateMessage(ServerConstants.LOGINFAILURE));
						oos.writeObject(new ValidateMessage(ServerConstants.LOGINFAILURE_NOTEXIST));
						oos.writeObject(new ValidateMessage(ServerConstants.LOGINFAILURE));
						oos.writeObject(new ValidateMessage(ServerConstants.LOGINFAILURE));
						oos.flush();
					}
				}else if(receiveMessage instanceof DefaultFeedMessage){
					DefaultFeedMessage dfm = (DefaultFeedMessage)receiveMessage;
					programServer.getSqlDriver().getDefaultAccount(dfm);
					//System.out.println("before send the default" + dfm.getAccounts().size());
					oos.writeObject(dfm);
					oos.flush();
				}else if(receiveMessage instanceof FavoriteFeedMessage){
					FavoriteFeedMessage dfm = (FavoriteFeedMessage)receiveMessage;
					
					programServer.getSqlDriver().getFavoriteFeeds(dfm);
					oos.writeObject(dfm);
					oos.flush();
	
				//start Messages
				}else if(receiveMessage instanceof StartChatMessage){
					StartChatMessage sc= (StartChatMessage)receiveMessage;
					myName = sc.getSender() + "chat";
					
					ArrayList<String> requestUser = programServer.getSqlDriver().getReq(sc.getSender());
					ArrayList<String> online = collectionThread.getOnlineUserNames(sc.getSender());
					//System.out.println("Online size" + online.size());
					
					oos.writeObject(new StartChatMessage(myName, online, requestUser));
					oos.flush();
				}else if(receiveMessage instanceof CleanMessage){
					CleanMessage cm = (CleanMessage)receiveMessage;
					programServer.getSqlDriver().removeReq(cm.getSender(), cm.getRceiver());
				}else if(receiveMessage instanceof ChatMessage){
					ChatMessage cm = (ChatMessage)receiveMessage;
					String sender = cm.getsendName();
					String receiver = cm.getreceiveName();
					//System.out.println("Inside" + myName + "receiver: " + receiver + " sender: " + sender);
					String Temp = receiver + "chat";
					Communicators access = collectionThread.getChatCommunicator(Temp);
					access.getOos().writeObject(cm);
					access.getOos().flush();
					//System.out.println("ChatMessage: " +"Sender: "+ cm.getsendName() +" Receiver: "+ cm.getreceiveName());
				
				}else if(receiveMessage instanceof SenderMessage){
					SenderMessage sm = (SenderMessage)receiveMessage;
					programServer.getSqlDriver().addReq(sm.getSender(), sm.getRceiver());
				}else if(receiveMessage instanceof UpdateFavoritesMessage ){
<<<<<<< HEAD
					System.out.println("Received the updatefavourites message");
=======
					System.out.println("C: received message from " + myName );
>>>>>>> e9c2b7566785ebe84b056ef7fc27094f18452f10
					UpdateFavoritesMessage fm = (UpdateFavoritesMessage)receiveMessage;
					boolean add = fm.isAdding();
					String addAccount = fm.getUsername();
					boolean exists = programServer.getSqlDriver().doesExist(addAccount);
					boolean fav_exist = programServer.getSqlDriver().ExistsFav(myName, addAccount);
					if(add==true){
						if(!exists){
							
							oos.writeObject(new FavoriteFeedbackMessage(ServerConstants.ADD_FAV_FAILURE_ACCOUNT_NOTEXISTS));
							oos.flush();
						}else{
							if(!fav_exist){//success
								programServer.getSqlDriver().addFav(myName, addAccount);
								System.out.println("C: success");
								oos.writeObject(new FavoriteFeedbackMessage(ServerConstants.ADD_FAV_SUCCESS));
								oos.flush();
							}else{//failure
								oos.writeObject(new FavoriteFeedbackMessage(ServerConstants.ADD_FAV_FAILURE_IN_FAV));
								oos.flush();
							}
						}
					}else{//remove
						if(!exists){
							oos.writeObject(new FavoriteFeedbackMessage(ServerConstants.REMOVE_FAV_FAILURE_ACCOUNT_NOTEXISTS));
							oos.flush();
						}else{
							
							if(!fav_exist){//failure
								//programServer.getSqlDriver().addFav(myName, addAccount);
								oos.writeObject(new FavoriteFeedbackMessage(ServerConstants.REMOVE_FAV_FAILURE_NOT_IN_FAV));
								oos.flush();
							}else{//success
								programServer.getSqlDriver().removeFav(myName, addAccount);
								oos.writeObject(new FavoriteFeedbackMessage(ServerConstants.REMOVE_FAV_SUCCESS));
								oos.flush();
							}
						}
						
					}
				}else if(receiveMessage instanceof AcceptMessage){
					AcceptMessage am = (AcceptMessage) receiveMessage;
					String sender = am.getSender();
					String receiver = am.getRceiver();
					//collectionThread.getChatCommunicator(sender).getOos().writeObject(am);
					//collectionThread.getChatCommunicator(sender).getOos().flush();
					//System.out.println("Inside" + myName + "receiver: " + receiver + " sender: " + sender);
					String Temp = sender + "chat";
					Communicators access = collectionThread.getChatCommunicator(Temp);
					
					access.getOos().writeObject(am);
					System.out.println("hahahah");
					access.getOos().flush();
				
					
					
					
					
					
					
				}else if(receiveMessage instanceof SendFavoritesMessage){
					SendFavoritesMessage fm = (SendFavoritesMessage)receiveMessage;
					programServer.getSqlDriver().getFavorites(myName, fm);
					oos.writeObject(fm);
					oos.flush();
				}else if(receiveMessage instanceof UpdateFeedMessage){
					UpdateFeedMessage ufm = (UpdateFeedMessage) receiveMessage;
					String uname = ufm.getUsername();
					String message = ufm.getMessage();
					String material = ufm.getMaterial();
					int quantity = ufm.getAmount();
					programServer.getSqlDriver().addFeed(uname, message, material, quantity);
					
				}else if(receiveMessage instanceof UpdateUserFeedMessage){
					//System.out.println("the server received updateUserFeedMessage");
					UpdateUserFeedMessage uufm = (UpdateUserFeedMessage)receiveMessage;
					programServer.getSqlDriver().getRecentFeeds(uufm);
					//System.out.println("size uufm: " + uufm.getAccounts().size() +"<ammount, material>"+uufm.getMaterials().size());
					//System.out.println("numPosts: " + uufm.getAmountOfPosts());
					oos.writeObject(uufm);
					oos.flush();
					
				}else if(receiveMessage instanceof SearchMessage){//when we receive the search message
					
					SearchMessage  sm = (SearchMessage)receiveMessage;
					if(programServer.getSqlDriver().doesExist(sm.getUsername())){
						Account ac = programServer.getSqlDriver().getAccount(sm.getUsername());
						sm.setAccount(ac);
						oos.writeObject(sm);
						oos.flush();
						
					}else{
						sm.setAccount(null);
						oos.writeObject(sm);
						oos.flush();
					}
				}else if(receiveMessage instanceof TestMessage){
					System.out.println("Succeed in test message");
					System.out.println("C: received message from " + myName );
					TestMessage fm = (TestMessage)receiveMessage;
					boolean add = fm.isAdding();
					String addAccount = fm.getUsername();
					boolean exists = programServer.getSqlDriver().doesExist(addAccount);
					boolean fav_exist = programServer.getSqlDriver().ExistsFav(myName, addAccount);
					if(add==true){
						if(!exists){
							
							oos.writeObject(new FavoriteFeedbackMessage(ServerConstants.ADD_FAV_FAILURE_ACCOUNT_NOTEXISTS));
							oos.flush();
						}else{
							if(!fav_exist){//success
								programServer.getSqlDriver().addFav(myName, addAccount);
								System.out.println("C: success");
								oos.writeObject(new FavoriteFeedbackMessage(ServerConstants.ADD_FAV_SUCCESS));
								oos.flush();
							}else{//failure
								oos.writeObject(new FavoriteFeedbackMessage(ServerConstants.ADD_FAV_FAILURE_IN_FAV));
								oos.flush();
							}
						}
				}
				}}
		} catch (EOFException eofe) {
			eofe.printStackTrace();
		}catch(SocketException se){ 
			se.printStackTrace();
		}catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			collectionThread.removeCommunicator(this);
		}
		

	}
	
	public String getmyName() {
		return myName;
	}

	public void setmyName(String comName) {
		this.myName = comName;
	}
}
