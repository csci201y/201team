package server;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.ServerSocket;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class ProgramServerGUI extends JFrame{
	private static final long serialVersionUID = 1;
	private JTextArea serverTextArea;
	private  JButton startButton;
	private  JButton stopButton;
	private  JScrollPane serverScrollPane;
	private int port;
	private ProgramServer server;
	private ServerSocket socket;
	
	public int getPort() {
		return port;
	}
	public ProgramServerGUI(ProgramServer server){
		super("Server");
		//parseServer();
		//socket = null;
		this.server = server;
		port = server.getPort();
		instantiateVariable();
		createGUI();
		addActions();
		
		System.out.println("I'm I here");
	}
	
	private void instantiateVariable(){
		serverTextArea = new JTextArea();
		startButton = new JButton("Start");
		stopButton = new JButton("Stop");
		serverScrollPane = new JScrollPane();
		//serverScrollPane.getVerticalScrollBar().setUI(new OfficeScrollBarUI());
		serverScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		serverScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
	}
	private void createGUI(){
		
		setVisible(true);
		setSize(350, 200);
		setLocation(500, 50);
		serverScrollPane.getViewport().add(serverTextArea);
		add(serverScrollPane,BorderLayout.CENTER);
		add(startButton, BorderLayout.SOUTH);
	}
	private void refreshText(){
		String line = "Server started on Port: " + port + "\n";
		serverTextArea.append(line);
		startButton.setVisible(false);
		stopButton.setVisible(true);
		ProgramServerGUI.this.add(stopButton, BorderLayout.SOUTH);
	}
	private void addActions(){
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		startButton.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				
				refreshText();
				server.serverConnection();
			}
			
		});
		
		stopButton.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				String st = "Server stopped." + "\n";
				serverTextArea.append(st);
				stopButton.setVisible(false);
				startButton.setVisible(true);
				ProgramServerGUI.this.add(startButton, BorderLayout.SOUTH);
				
				//socket
				server.closeServer();
			}
			
		});
	}
	public void addMessage(String msg) {
		//System.out.println("Not sppending test?");
		serverTextArea.append(msg +"\n");
	}
	
	public static void main(String [] args){
		
	}
}
