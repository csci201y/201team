package server;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Vector;

import com.mysql.jdbc.Driver;

import client.Account;
import message.DefaultFeedMessage;
import message.FavoriteFeedMessage;
import message.SendFavoritesMessage;
import message.UpdateUserFeedMessage;
//import com.mysql.jdbc.ResultSet;

public class SQLDriver {
	private final static String selectName = "SELECT * FROM ACCOUNTINFO WHERE ACCOUNTNAME=?";
	private final static String addAccount = "INSERT INTO ACCOUNTINFO(ACCOUNTNAME,PASSCODE, ACCOUNTTYPE, PROFILEPICNUM, CONTACT, DESCRIPTION) VALUES(?,?,?,?,?,?)";

	private final static String addFile = "INSERT INTO FILEINFO(USERNAME,FILENAME,ADDRESS) VALUES(?,?,?)";
	// deal with favoritelist
	private final static String addFav = "INSERT INTO FAVOURITEINFO(ACCOUNTNAME,FAVNAME) VALUES(?,?)";
	private final static String removeFav = "DELETE FROM FAVOURITEINFO WHERE ACCOUNTNAME=? AND FAVNAME=?";
	private final static String selectFav = "SELECT * FROM favouriteinfo WHERE ACCOUNTNAME=?";
	private final static String favExist = "SELECT * FROM FAVOURITEINFO WHERE ACCOUNTNAME=? AND FAVNAME=?";

	// deal with startchatMessage
	private final static String addReq = "INSERT INTO ChatRequest(Sender,Receiver) VALUES(?,?)";
	private final static String removeReq = "DELETE FROM ChatRequest WHERE Sender=? AND Receiver=?";
	private final static String getReqList = "SELECT * FROM ChatRequest WHERE Receiver=?";

	// deal with feed
	private final static String addFeed = "INSERT INTO FeedInfo(username, message,material, quantity) VALUES(?,?, ?, ?)";
	private final static String getRecentFeeds = "SELECT * FROM FeedInfo WHERE username = ? ORDER BY feedID DESC";

	private final static String getDefaultAccounts = "SELECT * FROM ACCOUNTINFO WHERE accountID=?";
	private final static String getFavAccounts = "SELECT * FROM FAVOURITEINFO WHERE ACCOUNTNAME=?";

	private Connection con;

	public SQLDriver() {
		try {
			new Driver();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void addAccount(String name, String passcode, String type, int picNum, String email, String dis) {
		connect();
		try {
			PreparedStatement ps = con.prepareStatement(addAccount);
			ps.setString(1, name);
			ps.setString(2, passcode);
			ps.setString(3, type);
			ps.setInt(4, picNum);
			//System.out.println("Is email a null?" + email);
			ps.setString(5, email);
			ps.setString(6, dis);
			ps.executeUpdate();
			// table with count 0");

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			stop();
		}
	}

	public void addFav(String account, String fav) {
		connect();
		try {
			PreparedStatement ps = con.prepareStatement(addFav);
			ps.setString(1, account);
			ps.setString(2, fav);
			ps.executeUpdate();
			// table with count 0");

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			stop();
		}
	}

	public boolean ExistsFav(String account, String fav) {
		connect();
		try {
			PreparedStatement ps = con.prepareStatement(favExist);
			ps.setString(1, account);
			ps.setString(2, fav);
			ResultSet result = ps.executeQuery();
			// table with count 0");
			if (result.next()) {
				// FactoryServerGUI.addMessage(result.getString(1)+" exists with
				// count: " + result.getInt(2));
				return true;

			} // return true;

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			stop();
		}

		return false;
	}

	public void addReq(String sender, String receiver) {
		connect();
		try {
			PreparedStatement ps = con.prepareStatement(addReq);
			ps.setString(1, sender);
			ps.setString(2, receiver);
			ps.executeUpdate();
			// table with count 0");
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			stop();
		}
	}

	public void removeReq(String sender, String receiver) {
		connect();
		try {

			PreparedStatement ps = con.prepareStatement(removeReq);
			ps.setString(1, sender);
			ps.setString(2, receiver);
			ps.executeUpdate();
			// table with count 0");
		} catch (SQLException e) {

		} finally {
			stop();
		}
	}

	public ArrayList<String> getReq(String receiver) {
		connect();
		ArrayList<String> myVector = new ArrayList<String>();
		try {
			PreparedStatement ps = con.prepareStatement(getReqList);
			ps.setString(1, receiver);
			ResultSet result = ps.executeQuery();
			while (result.next()) {
				String f = result.getString(2);
				myVector.add(f);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			stop();
		}

		return myVector;
	}

	public void removeFav(String account, String fav) {
		connect();
		try {
			PreparedStatement ps = con.prepareStatement(removeFav);
			ps.setString(1, account);
			ps.setString(2, fav);
			ps.executeUpdate();
			// table with count 0");

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			stop();
		}
	}

	public void addFile(String client, String fname, String fpath) {
		connect();
		try {
			PreparedStatement ps = con.prepareStatement(addFile);
			ps.setString(1, client);
			ps.setString(2, fname);
			ps.setString(3, fpath);
			ps.executeUpdate();
			// FactoryServerGUI.addMessage("Adding product:"+productName+" to
			// table with count 0");

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			stop();
		}
	}

	public void connect() {

		try {
			Class.forName("com.mysql.jdbc.Driver");
			con = DriverManager.getConnection("jdbc:mysql://localhost/accountinfo?user=root&password=root");
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException cnfe) {
			System.out.println("cnfe: " + cnfe.getMessage());
		}
	}

	public boolean doesExist(String productName) {
		connect();
		try {
			PreparedStatement ps = con.prepareStatement(selectName);
			ps.setString(1, productName);
			ResultSet result = ps.executeQuery();
			while (result.next()) {
				//System.out.println("The name: " + productName + " already exists");
				return true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			stop();
		}
		//System.out.println("The name: " + productName + " does not exist");
		return false;
	}

	public boolean password(String productName, String password) {
		connect();
		try {
			PreparedStatement ps = con.prepareStatement(selectName);
			ps.setString(1, productName);
			ResultSet result = ps.executeQuery();
			if (result.next()) {
				// FactoryServerGUI.addMessage(result.getString(1)+" exists with
				// count: " + result.getInt(2));
				//System.out.println("Password: " + result.getString(3));
				if (result.getString(3).equals(password)) {

					return true;
				}

			} // return true;
				// }
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			stop();
		}
		// FactoryServerGUI.addMessage("Unable to find product with name: "+
		// productName);
		// System.out.println("The name: " + productName + " does not exist");
		return false;
	}

	public void stop() {
		try {
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public Account getAccount(String name) {
		connect();
		// Vector<Account> myVector = new Vector<Account>();
		Account ac = null;
		try {

			PreparedStatement ps = con.prepareStatement(selectName);
			ps.setString(1, name);
			ResultSet result = ps.executeQuery();
			if (result.next()) {
				ac = new Account(result.getString(2), result.getString(3), result.getString(4), result.getInt(5));
				ac.setContact(result.getString(6));
				ac.setDescription(result.getString(7));
				return ac;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			stop();
		}

		// sfm.setFavorites(myVector);
		return null;
	}

	public void getFavorites(String name, SendFavoritesMessage sfm) {
		connect();
		Vector<Account> myVector = new Vector<Account>();
		try {

			PreparedStatement ps = con.prepareStatement(selectFav);
			ps.setString(1, name);
			ResultSet result1 = ps.executeQuery();
			while (result1.next()) {
				String temp = result1.getString(3);

				ps = con.prepareStatement(selectName);
				ps.setString(1, temp);

				ResultSet result = ps.executeQuery();
				if (result.next()) {
					System.out.println(result.getString(2));
					Account account = new Account(result.getString(2), result.getString(3), result.getString(4),
							result.getInt(5));
					account.setContact(result.getString(6));
					account.setDescription(result.getString(7));
					
					myVector.add(account);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			stop();
		}

		sfm.setFavorites(myVector);
	}

	// private final static String addFeed = "INSERT INTO FeedInfo(username,
	// message,material, quantity) VALUES(?,?, ?, ?)";

	public void addFeed(String username, String message, String material, int quantity) {
		connect();
		try {
			PreparedStatement ps = con.prepareStatement(addFeed);
			ps.setString(1, username);
			ps.setString(2, message);
			ps.setString(3, material);
			ps.setInt(4, quantity);
			ps.executeUpdate();
			// table with count 0");

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			stop();
		}
	}

	public void getRecentFeeds(UpdateUserFeedMessage uufm) {
		System.out.println("getRecentFeeds()");

		Vector<Account> account = uufm.getAccounts();
		Vector<Account> usernames = new Vector<Account>();
		int numPost = uufm.getAmountOfPosts();
		Vector<String> messages = new Vector<String>();
		Vector<String> materials = new Vector<String>();
		Vector<Integer> amounts = new Vector<Integer>();

		for (int i = 0; i < account.size(); ++i) {
			int count = 0;
			connect();

			System.out.println("\taccount size: " + account.size() + " username: " + account.get(i).getUsername());

			try {

				PreparedStatement ps = con.prepareStatement(getRecentFeeds);
				ps.setString(1, account.get(i).getUsername());
				ResultSet result = ps.executeQuery();

				while (result.next()) {
					if (count < numPost) {
						System.out.println(
								"\twhile loop " + count + " " + result.getString(3) + " " + result.getString(4));
						String me = result.getString(3);
						String ma = result.getString(4);
						Integer q = result.getInt(5);
						usernames.add(account.get(i));
						messages.add(me);
						materials.add(ma);
						amounts.add(q);
						count++;
					} else {
						break;
					}
				}
				int remain = numPost - count;
				for (int j = 0; j < remain; j++) {
					usernames.add(null);
					messages.add(null);
					materials.add(null);
					amounts.add(null);
				}

			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				stop();
			}

		}
		// return myVector;
		uufm.setAccounts(usernames);
		uufm.setMessages(messages);
		uufm.setMaterials(materials);
		uufm.setAmounts(amounts);
	}

	public void getDefaultAccount(DefaultFeedMessage dfm) {

		Vector<Account> myVector = new Vector<Account>();
		for (int i = 0; i < ServerConstants.numDefaultAccounts; ++i) {
			connect();
			try {

				PreparedStatement ps = con.prepareStatement(getDefaultAccounts);
				ps.setInt(1, i);
				ResultSet result = ps.executeQuery();
				while (result.next()) {
					Account account = new Account(result.getString(2), result.getString(3), result.getString(4),
							result.getInt(5));
					account.setContact(result.getString(6));
					account.setDescription(result.getString(7));
					myVector.add(account);
				}
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				stop();
			}
		}
		dfm.setAccounts(myVector);
	}

	public void getFavoriteFeeds(FavoriteFeedMessage ffm) {
		connect();
		ArrayList<Account> myVector = new ArrayList<Account>();
		try {

			PreparedStatement ps = con.prepareStatement(selectFav);
			String name = ffm.getName();
			ps.setString(1, name);
			ResultSet result = ps.executeQuery();
			while (result.next()) {
				Account account = new Account(result.getString(2), result.getString(3), result.getString(4),
						result.getInt(5));
				account.setContact(result.getString(6));
				account.setDescription(result.getString(7));
				myVector.add(account);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			stop();
		}
	}

	public void searchAccount(String name) {
		connect();
		try {
			PreparedStatement ps = con.prepareStatement(selectName);
			ps.setString(1, name);
			ResultSet result = ps.executeQuery();
			Account ac = null;
			if (result.next()) {
				// FactoryServerGUI.addMessage(result.getString(1)+" exists with
				ac = new Account(result.getString(2), result.getString(3), result.getString(4), result.getInt(5));
				ac.setContact(result.getString(6));
				ac.setDescription(result.getString(7));
			} // return true;
			ps.executeUpdate();
			// table with count 0");

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			stop();
		}

	}

	public static void main(String[] args) {
		SQLDriver msql = new SQLDriver();
		msql.connect();
		msql.addAccount("Aperson", "12fdsfs7", "type1", 2, "abb@usc.edu", "a discription");
		
		msql.stop();
	}
}
