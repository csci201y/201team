package server;

import java.io.IOException;
import java.net.BindException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Vector;

public class CollectionThread extends Thread{
	private ServerSocket ss;
	private Vector<Communicators> communicatorVector;
	private ProgramServer ps;
	public CollectionThread(ServerSocket ss, ProgramServer os) {
		this.ps = os;
		this.ss = ss;
		communicatorVector = new Vector<Communicators>();
	}
	
	public void removeCommunicator(Communicators oc) {
		communicatorVector.remove(oc);
	}
	public Communicators getChatCommunicator(String name) {
		for(int i=0; i<communicatorVector.size(); ++i){
			if(communicatorVector.get(i).getmyName().equals(name)){
				System.out.println("ChatThread Name: "+ communicatorVector.get(i).getmyName().equals(name));
				return communicatorVector.get(i);
			}
		}
		return null;
	}
	
	public void run() {

		try {
			while (true) {
				//System.out.println("Here again");
				System.out.println("Server waiting for connection...");
				Socket s = ss.accept();
				
				System.out.println("connection from" + s.getInetAddress());
				Communicators st = new Communicators(s, this, ps);
				communicatorVector.add(st);
			}
			
			
		}catch(BindException be ){  
			System.out.println("Port Already in use!");

		}catch(NullPointerException ne){
			System.out.println("Port Already in use!");
		}catch(SocketException se){
			//se.printStackTrace();
			//se.printStackTrace();
			
		}
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			for(Communicators c:communicatorVector){
				try {
					c.getSocket().close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

	}
	public void updateServerText(String s){
		
	}
	public void closeServerThread() {

		if (ss != null) {
			try {
				ss.close(); // ((Object) ss).shutDown();
				System.out.println("------------------Successfully closed!-------------------");
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}
	public ArrayList<String> getOnlineUserNames(String name){
		ArrayList<String> al = new ArrayList<String>();
		for(Communicators com : communicatorVector){
			if(com.getmyName()!=null){
				if(!com.getmyName().contains("chat")){
					if(!name.equals(com.getmyName())){	
						al.add(com.getmyName());
				
					}
				}
			}
		}
		return al;
	}
}
