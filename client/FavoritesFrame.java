package client;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import message.TestMessage;

public class FavoritesFrame extends JFrame{

	private static final long serialVersionUID = 1L;
	
	private JButton addButton, removeButton, cancelButton;
	private JTextField nameField;
	private ProgramClientListener pcl;

	public FavoritesFrame(ProgramClientListener pcl, Account account) {
		super();
		this.pcl = pcl;
		instantiateComponents();
		createGUI();
		addActions();
		setVisible(false);
		setResizable(false);
	}

	private void instantiateComponents() {
		nameField = new JTextField(25);
		addButton = new JButton("Add");
		removeButton = new JButton("Remove");
		cancelButton = new JButton("Cancel");
	}

	private void createGUI() {
		setSize(350, 150);
		setLocation(400, 400);
		
		ImageIcon logoIcon = new ImageIcon("src/resources/img/logo.png");
		JLabel logoLabel = new JLabel(logoIcon);
		JPanel imagePanel = new JPanel();
		imagePanel.setLayout(new BoxLayout(imagePanel, BoxLayout.X_AXIS));
		imagePanel.add(Box.createRigidArea(new Dimension(50,0)));
		imagePanel.add(logoLabel);
		imagePanel.add(Box.createRigidArea(new Dimension(50,0)));
		
		JPanel textPanel = new JPanel();
		textPanel.setLayout(new BoxLayout(textPanel, BoxLayout.X_AXIS));
		textPanel.add(nameField);
		
		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.X_AXIS));
		buttonPanel.add(Box.createRigidArea(new Dimension(10,0)));
		buttonPanel.add(addButton);
		buttonPanel.add(Box.createRigidArea(new Dimension(5,0)));
		buttonPanel.add(removeButton);
		buttonPanel.add(Box.createRigidArea(new Dimension(5,0)));
		buttonPanel.add(cancelButton);
		buttonPanel.add(Box.createRigidArea(new Dimension(10,0)));
		
		JPanel mainPanel = new JPanel();
		mainPanel.setLayout(new GridBagLayout());
		
		GridBagConstraints gbc = new GridBagConstraints();
		
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.gridwidth = 5;
		mainPanel.add(imagePanel, gbc);
		
		gbc.gridx = 0;
		gbc.gridy = 1;
		gbc.gridwidth = 5;
		mainPanel.add(textPanel, gbc);
		
		gbc.gridx = 0;
		gbc.gridy = 2;
		gbc.gridwidth = 5;
		mainPanel.add(buttonPanel, gbc);
		
		add(mainPanel);
	}

	private void addActions() {
		addButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				String username = nameField.getText();
				//UpdateFavoritesMessage message = new UpdateFavoritesMessage(username, true);
				TestMessage tm  = new TestMessage(username, true);
				pcl.sendMessage(tm);
			}
			
		});
		
		removeButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				String username = nameField.getText();
				UpdateFavoritesMessage message = new UpdateFavoritesMessage(username, false);
				pcl.sendMessage(message);

			}
			
		});
		
		cancelButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
			
		});
	}
	
}
