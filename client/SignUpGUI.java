package client;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Enumeration;

import javax.swing.AbstractButton;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JRadioButton;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import message.SignUpMessage;

public class SignUpGUI extends JDialog {
	
	private static final long serialVersionUID = 1L;
	private JButton cancelButton, signupButton;
	private Component locationComp;
	private JTextField usernameTextField;
	private JPasswordField passwordTextField, repeatTextField;
	private JRadioButton indivButton, computerButton, charityButton;
	private JRadioButton female1, male1, male2, female2;
	private JRadioButton heart, computer, flower, spaceshuttle, robot;
	private JRadioButton paw, tractor, apple;
	private JLabel female1Label, male1Label, male2Label, female2Label;
	private JLabel heartLabel, computerLabel, flowerLabel, spaceshuttleLabel, robotLabel;
	private JLabel pawLabel, tractorLabel, appleLabel;
	private JPanel radioButtonPanel, picturePanel;
	private JLabel descLabel, emailLabel;
	private JTextField emailTextField;
	private JTextArea descArea;
	private ButtonGroup group1, group2;
	private SignUpMessage info;

	private ProgramClientListener pcl;

	public SignUpGUI(Frame frame, ProgramClientListener pcl) {
		super(frame, new String("Sign Up"), true);

		this.pcl = pcl;
		initializeVariables();
		createGUI();
		addActions();
		setResizable(false);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
	}

	private void initializeVariables() {
		cancelButton = new JButton("Cancel");
		signupButton = new JButton("Sign Up");
		
		radioButtonPanel = new JPanel();
		JPanel labelPanel = new JPanel();
		labelPanel.add(new JLabel(""));
		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.X_AXIS));
		radioButtonPanel.setLayout(new BoxLayout(radioButtonPanel, BoxLayout.Y_AXIS));
		indivButton = new JRadioButton("Individual");
		computerButton = new JRadioButton("Business");
		charityButton = new JRadioButton("Charity");
		buttonPanel.add(indivButton);
		buttonPanel.add(computerButton);
		buttonPanel.add(charityButton);
		radioButtonPanel.add(labelPanel);
		radioButtonPanel.add(buttonPanel);
		group1 = new ButtonGroup();
		group1.add(indivButton);
		group1.add(computerButton);
		group1.add(charityButton);
		
		picturePanel = new JPanel();
		
		ImageIcon male1Image = new ImageIcon("src/resources/img/male1.png");
		male1 = new JRadioButton("");
		male1Label = new JLabel(male1Image);
		ImageIcon female1Image = new ImageIcon("src/resources/img/female1.png");
		female1 = new JRadioButton("");
		female1Label = new JLabel(female1Image);
		ImageIcon male2Image = new ImageIcon("src/resources/img/male2.png");
		male2 = new JRadioButton("");
		male2Label = new JLabel(male2Image);
		ImageIcon female2Image = new ImageIcon("src/resources/img/female2.png");
		female2 = new JRadioButton("");
		female2Label = new JLabel(female2Image);
		ImageIcon heartImage = new ImageIcon("src/resources/img/heart.png");
		heart = new JRadioButton("");
		heartLabel = new JLabel(heartImage);
		ImageIcon computerImage = new ImageIcon("src/resources/img/computer.png");
		computer = new JRadioButton("");
		computerLabel = new JLabel(computerImage);
		ImageIcon flowerImage = new ImageIcon("src/resources/img/flower.png");
		flower = new JRadioButton("");
		flowerLabel = new JLabel(flowerImage);
		ImageIcon spaceshuttleImage = new ImageIcon("src/resources/img/spaceshuttle.png");
		spaceshuttle = new JRadioButton("");
		spaceshuttleLabel = new JLabel(spaceshuttleImage);
		ImageIcon robotImage = new ImageIcon("src/resources/img/robot.png");
		robot = new JRadioButton("");
		robotLabel = new JLabel(robotImage);
		ImageIcon pawImage = new ImageIcon("src/resources/img/paw.png");
		paw = new JRadioButton("");
		pawLabel = new JLabel(pawImage);
		ImageIcon tractorImage = new ImageIcon("src/resources/img/tractor.png");
		tractor = new JRadioButton("");
		tractorLabel = new JLabel(tractorImage);
		ImageIcon appleImage = new ImageIcon("src/resources/img/apple.png");
		apple = new JRadioButton("");
		appleLabel = new JLabel(appleImage);
		
		descLabel = new JLabel("Description: ");
		descArea = new JTextArea(10, 30);
		descArea.setLineWrap(true);
		
		emailLabel = new JLabel("    Email Address: ");
		emailTextField = new JTextField(15);
		
		picturePanel.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		
		JPanel blankPanel = new JPanel();
		c.gridx = 0;
		c.insets = new Insets(2, 2, 2, 5);
		c.gridwidth = 16;
		c.gridy = 0;
		picturePanel.add(blankPanel, c);
		
		
		c.gridx = 0;
		c.gridwidth = 2;
		c.gridy = 1;
		picturePanel.add(male1, c);
		
		c.gridx = 2;
		picturePanel.add(male1Label, c);
		
		c.gridx = 4;
		picturePanel.add(female1, c);
		
		c.gridx = 6;
		picturePanel.add(female1Label, c);
		
		c.gridx = 8;
		picturePanel.add(male2, c);
		
		c.gridx = 10;
		picturePanel.add(male2Label, c);
		
		c.gridx = 0;
		c.gridwidth = 2;
		c.gridy = 2;
		picturePanel.add(female2, c);
		
		c.gridx = 2;
		picturePanel.add(female2Label, c);
		
		c.gridx = 4;
		picturePanel.add(paw, c);
		
		c.gridx = 6;
		picturePanel.add(pawLabel, c);
		
		c.gridx = 8;
		picturePanel.add(spaceshuttle, c);
		
		c.gridx = 10;
		picturePanel.add(spaceshuttleLabel, c);
		
		c.gridx = 0;
		c.gridwidth = 2;
		c.gridy = 3;
		picturePanel.add(flower, c);
		
		c.gridx = 2;
		picturePanel.add(flowerLabel, c);
		
		c.gridx = 4;
		picturePanel.add(computer, c);
		
		c.gridx = 6;
		picturePanel.add(computerLabel, c);
		
		c.gridx = 8;
		picturePanel.add(robot, c);
		
		c.gridx = 10;
		picturePanel.add(robotLabel, c);
		
		c.gridx = 0;
		c.gridy = 4;
		picturePanel.add(heart, c);
		
		c.gridx = 2;
		picturePanel.add(heartLabel, c);
		
		c.gridx = 4;
		picturePanel.add(apple, c);
		
		c.gridx = 6;
		picturePanel.add(appleLabel, c);
		
		c.gridx = 8;
		picturePanel.add(tractor, c);
		
		c.gridx = 10;
		picturePanel.add(tractorLabel, c);
		
		group2 = new ButtonGroup();
		group2.add(male1);
		group2.add(female1);	
		group2.add(male2);
		group2.add(female2);
		group2.add(paw);
		group2.add(spaceshuttle);
		group2.add(flower);
		group2.add(computer);
		group2.add(robot);
		group2.add(heart);
		group2.add(apple);
		group2.add(tractor);
	}
	
	private void createGUI() {
				
		ImageIcon logo = new ImageIcon("src/resources/img/logo.png");
		JLabel logoLabel = new JLabel(logo);
		JPanel topPanel = new JPanel();
		topPanel.setLayout(new BoxLayout(topPanel, BoxLayout.X_AXIS));
		topPanel.add(Box.createRigidArea(new Dimension(15,0)));
		topPanel.add(logoLabel);
		topPanel.add(Box.createRigidArea(new Dimension(15,0)));

		JLabel usernameLabel = new JLabel("Username:");
		usernameTextField = new JTextField(20);
		
		JLabel passwordLabel = new JLabel("Password:");
		passwordTextField = new JPasswordField(20);

		JLabel repeatLabel = new JLabel("Confirm:");
		repeatTextField = new JPasswordField(20);

		Container contentPane = getContentPane();
		
		contentPane.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		
		c.gridx = 0;
		c.gridwidth = 16;
		c.gridy = 1;
		c.insets = new Insets(2, 2, 2, 5);
		contentPane.add(topPanel, c);
		
		c.gridx = 4;
		c.gridwidth = 4;
		c.gridy = 2;
		contentPane.add(usernameLabel, c);
		
		c.gridx = 8;
		c.gridy = 2;
		contentPane.add(usernameTextField, c);
		
		c.gridx = 4;
		c.gridy = 3;
		contentPane.add(passwordLabel, c);
		
		c.gridx = 8;
		c.gridy = 3;
		contentPane.add(passwordTextField, c);
		
		c.gridx = 4;
		c.gridy = 4;
		contentPane.add(repeatLabel, c);
		
		c.gridx = 8;
		contentPane.add(repeatTextField, c);
		
		c.gridx = 0;
		c.gridwidth = 16;
		c.gridy = 5;
		contentPane.add(radioButtonPanel, c);
		
		c.gridx = 0;
		c.gridwidth = 16;
		c.gridy = 6;
		contentPane.add(picturePanel, c);
		
		c.gridx = 4;
		c.gridwidth = 2;
		c.gridy = 7;
		contentPane.add(emailLabel, c);
		
		c.gridx = 8;
		contentPane.add(emailTextField, c);
		
		c.gridx = 4;
		c.gridy = 8;
		contentPane.add(descLabel, c);
		
		JPanel descPanel = new JPanel();
		descPanel.add(descArea);
		c.gridx = 0;
		c.gridwidth = 16;
		c.gridy = 9;
		contentPane.add(descPanel, c);
		
		c.gridx = 8;
		c.gridwidth = 2;
		c.gridy = 10;
		contentPane.add(signupButton, c);
		
		c.gridx = 10;
		contentPane.add(cancelButton, c);

		pack();
		setLocationRelativeTo(locationComp);
	
	}
	
	private void addActions() {
		signupButton.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				String username = usernameTextField.getText();
				String password = new String(passwordTextField.getPassword());
				String repeat = new String(repeatTextField.getPassword());
				if (repeat.equals(password)) {
					if (valid(password)) {
						info = new SignUpMessage(username, encrypt(password), getSelectedAccount(group1), 
								getSelectedPicture(group2));
						info.setDescription(descArea.getText());
						info.setEmail(emailTextField.getText());
						System.out.println("PP chosen: " + info.getProfilePicture());
						pcl.sendMessage(info);
					} else {
						JOptionPane.showMessageDialog(null, "Password must contain at least: " + '\n'
								+ "  1-Number 1-Uppercase Letter", "Sign-up Failed", 
								JOptionPane.WARNING_MESSAGE);
					}
				} else {
					JOptionPane.showMessageDialog(null, "Passwords do not match.", "Sign-up Failed", 
							JOptionPane.WARNING_MESSAGE);
				}

			}
			
		});
		
		cancelButton.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				dispose();
			}
			
		});
	}
	
	private String encrypt(String password) {
		
        String generatedPassword = null;
        
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(password.getBytes());
            byte[] bytes = md.digest();
            StringBuilder sb = new StringBuilder();
            for(int i=0; i< bytes.length ;i++)
            {
                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
            }
            generatedPassword = sb.toString();
        } 
        catch (NoSuchAlgorithmException nsae) 
        {
            System.out.println("nsae: " + nsae.getMessage());
        }
        
        return generatedPassword;
	}
	
	public boolean valid(String password) {
		for (char c : password.toCharArray()) {
            if (Character.isDigit(c)) {
            	{
            		if (!password.equals(password.toLowerCase())) {
            			return true;
            		}
            	}
            }
        }
		return false;
	}
	
    public String getSelectedAccount(ButtonGroup buttonGroup) {
        for (Enumeration<AbstractButton> buttons = buttonGroup.getElements(); buttons.hasMoreElements();) {
            AbstractButton button = buttons.nextElement();

            if (button.isSelected()) {
                return button.getText();
            }
        }

        return null;
    }
    
    public int getSelectedPicture(ButtonGroup buttonGroup) {
    	int count = 1;
        for (Enumeration<AbstractButton> buttons = buttonGroup.getElements(); buttons.hasMoreElements();) {
            AbstractButton button = buttons.nextElement();
            
            if (button.isSelected()) {
                return count;
            }
            count++;
        }

        return -1;
    }

}
