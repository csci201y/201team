package client;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.Socket;
import java.net.SocketException;
import java.util.Scanner;
import javax.swing.JOptionPane;

public class ProgramClient {
	
	public ProgramClient(String hostname, int port) {
		Socket s = null;
		try {
			s = new Socket(hostname, port);
			new MainGUI(s);
		}catch(SocketException se){
			System.out.println("Server Disconnected!");
		}catch (IOException ioe) {
			System.out.println("ProgramClient Exception: " + ioe.getMessage());
			JOptionPane.showMessageDialog(null, 
										  "Connection to server failed",
										  "Failed Connection",
										  JOptionPane.ERROR_MESSAGE);
		}
	}
	
	private static ClientConfig parseClientConfigFile() {
		Scanner fin = null;
		try {
			fin = new Scanner(new FileReader(Constants.CLIENT_CONFIG_PATH));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		Scanner sin = new Scanner(fin.nextLine());
		sin.skip(Constants.SERVER_IP_TAG);
		String serverIP = sin.next();
		Scanner pin = new Scanner(fin.nextLine());
		pin.skip(Constants.PORT_TAG);
		int port = pin.nextInt();
		fin.close();
		sin.close();
		pin.close();
		ClientConfig cc = new ClientConfig(serverIP, port);
		return cc;
	}
	
	public static void main(String[] args) {
		ClientConfig cc = parseClientConfigFile();
		new ProgramClient(cc.getServerIP(), cc.getPort());
	}
	
}

class ClientConfig {
	private String serverIP;
	private int port;
	
	public ClientConfig(String serverIP, int port) {
		this.serverIP = serverIP;
		this.port = port;
	}
	
	public String getServerIP() {
		return serverIP;
	}
	
	public int getPort() {
		return port;
	}
}
