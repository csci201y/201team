package client;

import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.Socket;
import java.util.Scanner;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextField;

import message.SearchMessage;

public class TopBarPanel extends JPanel {
	
	private static final long serialVersionUID = 1L;
	private JTextField searchField;
	private JButton searchButton, homeButton, messageButton, profileButton, calculatorButton;
	private MakePostButton makePostButton;
	private JButton favoritesButton;
	private ProgramClientListener sa;
	private Account account;
	private MainGUI main;
	private FavoritesFrame ff;

	public TopBarPanel(ProgramClientListener sa, Account account, MainGUI main) {
		this.account = account;
		this.sa = sa;
		this.main = main;
		ff = new FavoritesFrame(sa, account);
		instantiateComponents();
		createGUI();
		addActions();
		
	}

	private void instantiateComponents() {
		ImageIcon logo = new ImageIcon("src/resources/img/logo.png");
		homeButton = new JButton("", logo);
		homeButton.setOpaque(false);
		homeButton.setFocusPainted(false);
		homeButton.setBorderPainted(false);
		homeButton.setContentAreaFilled(false);
		
		searchField = new JTextField(100);
		searchField.setPreferredSize(new Dimension(300, 25));
		searchField.setMaximumSize(searchField.getPreferredSize());
		searchButton = new JButton("Search");
		
		ImageIcon makePostImage = new ImageIcon("src/resources/img/writePost.png");
		makePostButton = new MakePostButton(makePostImage, account.getUsername());
		makePostButton.setOpaque(false);
		makePostButton.setFocusPainted(false);
		makePostButton.setBorderPainted(false);
		makePostButton.setContentAreaFilled(false);
		makePostButton.setProgramClientListener(sa);
		
		ImageIcon favoritesImage = new ImageIcon("src/resources/img/favorites.png");
		favoritesButton = new JButton("", favoritesImage);
		favoritesButton.setOpaque(false);
		favoritesButton.setFocusPainted(false);
		favoritesButton.setBorderPainted(false);
		favoritesButton.setContentAreaFilled(false);
		
		ImageIcon calculatorImage = new ImageIcon("src/resources/img/calculator.png");
		calculatorButton = new JButton("", calculatorImage);
		calculatorButton.setOpaque(false);
		calculatorButton.setFocusPainted(false);
		calculatorButton.setBorderPainted(false);
		calculatorButton.setContentAreaFilled(false);
		
		ImageIcon messageImage = new ImageIcon("src/resources/img/message.png");
		messageButton = new JButton("", messageImage);
		messageButton.setOpaque(false);
		messageButton.setFocusPainted(false);
		messageButton.setBorderPainted(false);
		messageButton.setContentAreaFilled(false);
		
		ImageIcon profileImage = new ImageIcon("src/resources/img/profile.png");
		profileButton = new JButton("", profileImage);	
		profileButton.setOpaque(false);
		profileButton.setFocusPainted(false);
		profileButton.setBorderPainted(false);
		profileButton.setContentAreaFilled(false);
	}

	private void createGUI() {
		setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
		
		add(Box.createRigidArea(new Dimension(10, 0)));
		add(homeButton);
		add(Box.createRigidArea(new Dimension(170, 0)));
		add(searchField);
		add(searchButton);
		add(Box.createRigidArea(new Dimension(150, 0)));
		add(makePostButton);
		add(favoritesButton);
		add(calculatorButton);
		add(messageButton);
		add(profileButton);
		add(Box.createRigidArea(new Dimension(10, 0)));
		
		setBackground(new Color(35, 81, 127));
	}
	
	public MainGUI getMain(MainGUI main)
	{
		return main;
	}

	private void addActions() {
		
		homeButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				main.getFeed().updateFeed();
				((CardLayout) main.getCards().getLayout()).show(main.getCards(), "Feed");
				searchField.setText("");
			}
			
		});
		
		searchButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				SearchMessage mess = new SearchMessage(searchField.getText());
				sa.sendMessage(mess);
				//SearchResultPane searchResult = new SearchResultPane(searchField.getText(), sa);
				//main.switchToSearch(searchResult);
			}
			
		});
		
		
		
		calculatorButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				TaxDeductionPanel taxPanel = new TaxDeductionPanel();
				main.switchToTax(taxPanel);
				searchField.setText("");
			}
			
		});
		
		messageButton.addActionListener(new ActionListener() {

	
				@Override
				public void actionPerformed(ActionEvent e) {
					System.out.println("name is " + account.getUsername());
					//new ChatThread(sa, account.getUsername());
					
					Scanner fin = null;
					try {
						fin = new Scanner(new FileReader(Constants.CLIENT_CONFIG_PATH));
					} catch (FileNotFoundException e1) {
						e1.printStackTrace();
					}
					Scanner sin = new Scanner(fin.nextLine());
					sin.skip(Constants.SERVER_IP_TAG);
					String serverIP = sin.next();
					Scanner pin = new Scanner(fin.nextLine());
					pin.skip(Constants.PORT_TAG);
					int port = pin.nextInt();
					fin.close();
					sin.close();
					pin.close();
					System.out.println("SeverIP " + serverIP + "Port " + port);
					
					Socket m;
					try {
						
						m = new Socket(serverIP, port);
						new ChatThread(m, account.getUsername());
					} catch (IOException e1) {
						e1.printStackTrace();
					}
					 
			}
			
		});
		
		profileButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				System.out.println("action account des: " + main.getAccount().getDescription());
				ProfilePanel myPanel = new ProfilePanel(main.getAccount());
				main.switchToProfile(myPanel);
				searchField.setText("");
			}
			
		});
		
		favoritesButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				ff.setVisible(true);
			}
			
		});
		
	}
	
	public void setAccount(Account account) {
		this.account = account;
	}
	
	public FavoritesFrame getFavoritesFrame() {
		return ff;
	}

}
