package client;

public class BusinessAccount extends Account {
	
	BusinessAccount(String user, String pass)
	{
		super(user, pass);
		super.setCanDonate(true);
		super.setCanChat(true);
		super.setCanReceiveDonations(false);
	}
	
	/*public double calculateTaxDeduction() //
	{ 
		return 1.0;
	} */

}
