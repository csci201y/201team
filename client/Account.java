package client;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Vector;
import java.awt.Image;
import java.awt.Toolkit;
import java.io.*;
import javax.swing.*;

import message.DefaultFeedMessage;
import message.FavoriteFeedMessage;

public class Account implements Serializable {

	private String password; //encrypted
	private String username;
	private JLabel profilePicture;
	private Vector<Account> defaultFeedAccounts;
	private Vector<Account> favoriteAccounts;
	private ProgramClientListener pcl;
	private boolean ableToChat = false, ableToDonate = false, ableToReceiveDonations;
	private String descrip, contact;
	private int profilePicNum;
	private String accountType;

	/*LIST OF METHODS
	 * 
	 * Account(String, String, String, int, Vector<Account>) -> constructor for the server side
	 * Account(String, String)
	 * public String getEncrypted(String) -> encrypts user's password
	 * public void setUpStreams(ObjectOutputStream, ObjectInputStream) -> sets output/input streams to/from server
	 * public String getUsername()
	 * public String getPassword()
	 * public void setProfilePicture(int)
	 * public void sendFavoriteList() -> sends current favorite list to the server
	 * public boolean addFavorite(Account) -> only returns false if acc was already favorited
	 * public boolean removeFavorite(Account) -> only returns false if acc isn't a favorite
	 * public boolean isFavoritesGreaterThanDefault() -> returns if there are more favorited accounts than default accounts
	 * ----THE FOLLOWING METHODS HELP DIFFERENTIATE BETWEEN ACCOUNT TYPES-----
	 * public void setCanChat(boolean)
	 * public boolean getCanChat()
	 * public void setCanDonate(boolean)
	 * public boolean getCanDonate()
	 * public void setCanReceiveDonations(boolean)
	 * public boolean getCanReceiveDonations()
	 * -----------------------------------------------------------------------
	 * public void updateAsk(String) -> String parameter represents the action, or ask, that the user took
	 * public String getAsk() 
	 * public void setDescription(String) -> allows the user to set account description
	 * public String getDescription()
	 * 
	 */

	public Account(String accountName, String passcode, String accountType, int profilePicNum)
	{
		username = accountName;
		password = passcode;
		this.profilePicNum = profilePicNum;
		this.accountType = accountType;
		
		if(accountType.equals("Charity"))
		{
			setCanChat(true);
			setCanReceiveDonations(true);
			setCanDonate(false);
		} else if(accountType.equals("Individual"))
		{
			setCanDonate(true);
			setCanChat(true);
			setCanReceiveDonations(false);
		} else if(accountType.equals("Business"))
		{
			setCanDonate(true);
			setCanChat(true);
			setCanReceiveDonations(false);
		} else if(accountType.equals("Visitor"))
		{
			setCanChat(false);
			setCanDonate(false);
			setCanReceiveDonations(false);
		}
		
	}
	
	public Account(String name, String pass) {
		username = name;
		password = pass;
		defaultFeedAccounts = new Vector<Account>();
		favoriteAccounts = new Vector<Account>();
	}
	
	public Account(String name, String pass, ProgramClientListener pcl)
	{
		this(name, pass);
		this.pcl = pcl;
		populateDefaultFeedAccounts();
		populateFavoriteAccounts();
	}
	
	public void setProgramClientListener(ProgramClientListener pcl) {
		this.pcl = pcl;
	}
	
	public String getUsername()
	{
		return username;
	}
	
	public String getPassword()
	{
		return password;
	}
	
	/*public void updateAsk(String a)
	{
		actions.add(a);
	}
	
	public String getMostRecentAsk()
	{
		if(!(actions.size() > 0)) return null;
		else return actions.get(actions.size() - 1);
	} */
	
	public void setContact(String contact)
	{
		this.contact = contact;
	}
	
	public String getContact()
	{
		return contact;
	}
	
	public void setDescription(String description)
	{
		descrip = description;
	}
	
	public String getDescription()
	{
		return descrip;
	}
	
	//when the GUI calls this method, it will convert the user's choice into an int
	public void setProfilePicture(int choice) 
	{
		profilePicNum = choice;
	}
	
	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	public int getProfilePicture()
	{
		return profilePicNum;
	}
	
	//returns true if the operation is successful, returns false if the account is already favorited
	public boolean addFavorite(Account acc)
	{
		if(favoriteAccounts.contains(acc)) return false; 
		else 
		{
			favoriteAccounts.add(acc);
			UpdateFavoritesMessage add = new UpdateFavoritesMessage(acc.getUsername(), true);
			pcl.sendMessage(add);
			return true;
		}
	}
	
	//returns false if the account was not a favorite, true if the operation was successful
	public boolean removeFavorite(Account acc)
	{
		if(!favoriteAccounts.contains(acc)) return false;
		else
		{
			favoriteAccounts.remove(acc);
			UpdateFavoritesMessage remove = new UpdateFavoritesMessage(acc.getUsername(), false);
			if (pcl != null)
				pcl.sendMessage(remove);
			return true;
		}
	}

	//sends message to the server
	public void populateFavoriteAccounts()
	{
			pcl.sendMessage(new FavoriteFeedMessage(username, favoriteAccounts));
	}
	
	//receives message from the server (through programClientListener)
	public void updateFavoriteAccounts(FavoriteFeedMessage message) {
		favoriteAccounts = message.getFavorites();
	}
	
	//sends message to the server
	public void populateDefaultFeedAccounts()
	{
		pcl.sendMessage(new DefaultFeedMessage(defaultFeedAccounts, true));
	}
	
	//receives message from the server (through programClientListener)
	public void updateDefaultFeedAccounts(DefaultFeedMessage message) {
		if(Constants.DEBUGGING_FEED)System.out.println("inside populateDefaultAccounts received message size: " + message.getAccounts().size());
		defaultFeedAccounts = message.getAccounts();
	}
	
	public Vector<Account> getDefaultFeedAccounts()
	{
		return defaultFeedAccounts;
	}
	
	public Vector<Account> getFavoriteAccounts()
	{
		return favoriteAccounts;
	}
	
	public boolean isFavoritesGreaterThanDefault()
	{
		return (favoriteAccounts.size() >= defaultFeedAccounts.size());
	}
	
	public void setCanChat(boolean yes)
	{
		ableToChat = yes;
	}
	
	public boolean getCanChat()
	{
		return ableToChat;
	}
	
	public void setCanDonate(boolean yes)
	{
		ableToDonate = yes;
	}
	
	public boolean getCanDonate()
	{
		return ableToDonate;
	}
	
	public void setCanReceiveDonations(boolean yes)
	{
		ableToReceiveDonations = yes;
	}
	
	public boolean getCanReceiveDonations()
	{
		return ableToReceiveDonations;
	}
}
