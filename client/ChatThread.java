package client;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.ArrayList;

import message.AcceptMessage;
import message.ChatMessage;
import message.CleanMessage;
import message.LoginMessage;
import message.SenderMessage;
import message.StartChatMessage;

public class ChatThread extends Thread{
	
	 
	private ChatFrame CF;
	private ObjectInputStream ois;
	private ObjectOutputStream oos;
	private ArrayList<String> Allname;
	private ArrayList<String> RequestChatname;
	private Socket s;
	private ChatMessage message;
	private boolean test = false;
	
	private String user;
	private ChatHistoryScrollPane csp;
	 
	
    public ChatThread(Socket s, String user){
    	//only send and read RequestChatMessage and ChatMessage 
    	 
    	try{
    		this.user = user;
    		this.s =s;
			oos = new ObjectOutputStream(s.getOutputStream());
			ois = new ObjectInputStream(s.getInputStream());
			StartChatMessage scm = new StartChatMessage(user, Allname, RequestChatname);//tell the server please give me back all the username and all the people who request to talk with me 
			System.out.println(scm.getSender());
			oos.writeObject(scm);
			oos.flush();	
			
			csp = new ChatHistoryScrollPane(null, null, null, this);
			csp.setVisible(false);
			
			this.start();
			
    	}catch(SocketException se){
			 System.out.println("connection failed");
		 }
    	catch (IOException ioe) {
			System.out.println("ioe11: " + ioe.getMessage());
		} 
    	 
    }
    
    public void run() {
    	try{
    	  while(true) {
    	    Object receiveMessage = ois.readObject();
    	    if(receiveMessage instanceof StartChatMessage){ //if you received to chat success, now you need to get all the three
    	    	test = true;
    	    	System.out.println("I receive a StartChatMessage");
    	    	Allname = ((StartChatMessage) receiveMessage).getAllChatname();
    	    	RequestChatname = ((StartChatMessage) receiveMessage).getRequestChatname();
    	    	//System.out.println("Request size " + RequestChatname.size());
    	    	//get all three  
    	    	CF = new ChatFrame(this, user);
        		System.out.println(user);
    	    }
    	    
    	   
    	    if(receiveMessage instanceof AcceptMessage){
    	    	//System.out.println("I am receiving accept " + ((AcceptMessage)receiveMessage).getSender());
    	    	if(((AcceptMessage)receiveMessage).getSender().equals(user)){
    	    	String caller = ((AcceptMessage)receiveMessage).getRceiver(); //when some agree to chat with you and you open the chatscrollpane 
    	    	//csp = new ChatHistoryScrollPane( caller,user, null, this);
    	    	csp.setClean();
    	    	csp.setUser(user);
    	    	csp.setCaller(caller);
    	    	csp.updateJLabel(caller);
    	    	csp.setVisible(true);
    	    	}
    	    }
    	    if(receiveMessage instanceof ChatMessage){
			message = (ChatMessage) receiveMessage; //keep reading the chat information 
			System.out.println(message.getsendName() + " : " + message.getMessage());
			if(message.getreceiveName().equals(user)){ // if it's a message sent to the user 
	    		 String newmess = message.getMessage(); //get new message and update 
	    		   System.out.println("CM(newmess): " + newmess);
	    		 	csp.updateTextDisplay(newmess);
	    		 
			} 
			 
		   }
    	  }
    	}catch (ClassNotFoundException cnfe) {
			System.out.println("cnfe: " + cnfe.getMessage());
		} catch (IOException ioe) {
			System.out.println("ioe: " + ioe.getMessage());
		}
    }
    
    public ChatHistoryScrollPane getCSP(){
    	return csp;
    }
    
    public boolean getTest(){
    	return test;
    }
    
    public ArrayList<String> getAllname(){ //get the naem for all the user 
    	return Allname;
    }
    
    public ArrayList<String> getRequestChatname(){  //get all the user who sent request and talk with you before
    	//System.out.println("11Request size " + RequestChatname.size());
    	return RequestChatname;
    }
	
    
    public void sendChatMessage(ChatMessage CM){ //send the chatmessage 
    	try {
    		System.out.println("Start to send ChatMessagre" + CM.getsendName() + " MESSGAE: " + CM.getMessage());
			oos.writeObject(CM);
			oos.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	 
    }
    
    public void sendSenderMessage(SenderMessage SM){
    	try {
			oos.writeObject(SM);
			oos.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	 
    }
    
    public void sendAcceptMessage(AcceptMessage SM){
    	try {
    		System.out.println(" SM " + SM.getSender());
			oos.writeObject(SM);
			oos.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	 
    }
    
    public void sendCleanMessage(CleanMessage SM){
    	try {
    		System.out.println("we are sending");
			oos.writeObject(SM);
			oos.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	 
    }
    
    public ChatMessage getChatMessage(){
    	return message;
    }
    
    /*public static void main(String [] args) {
 		 Socket m = null;
		try {
			m = new Socket("localhost", 6789);
			new ChatThread(m, "yang");
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	 
 	}  */ 
}
