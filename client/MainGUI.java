package client;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.net.Socket;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class MainGUI extends JFrame {
	private static final long serialVersionUID = 1;
	
	private NotLoggedInTopBarPanel notLoggedInTopBarPanel;
	private TopBarPanel topBarPanel;
	private Feed feed;
	private Socket s;
	private Account account;
	private CardLayout cards;
	private JPanel hasCards;
	private ProgramClientListener pcl;
	
	public MainGUI(Socket socket) {
		super("Shareity");
		this.s = socket;
		instantiateComponents();
		createGUI();
		setLocationRelativeTo(null);
		setVisible(true);
		setFocusable(true);
	}
	
	private void instantiateComponents() {
		pcl = new ProgramClientListener(this, s);
		notLoggedInTopBarPanel = new NotLoggedInTopBarPanel(pcl, this);
		Account temp = new Account("username", "password", pcl);
		account = temp;
		feed = new Feed(temp, pcl);
	}
	
	private void createGUI() {
		setSize(950, 650);
		setLocation(400, 100);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		cards = new CardLayout();
		hasCards = new JPanel(cards);
		hasCards.setBackground(Color.LIGHT_GRAY);
		
		JPanel forFeed = new JPanel();
		forFeed.add(feed, BorderLayout.CENTER);
		hasCards.add(feed, "Feed");
				
		add(notLoggedInTopBarPanel, BorderLayout.NORTH);
		//add(feed, BorderLayout.CENTER);
		add(hasCards, BorderLayout.CENTER);
	}
	
	public void passToTopBar(MainGUI a)
	{
		topBarPanel.getMain(a);
	}
	
	public void switchToSearch(SearchResultPane searchPane) {
		hasCards.add(searchPane, "Search Panel");
		((CardLayout) hasCards.getLayout()).show(hasCards, "Search Panel");
	}
	
	public void switchToProfile(ProfilePanel panel) {
		hasCards.add(panel, "Profile Panel");
		((CardLayout) hasCards.getLayout()).show(hasCards, "Profile Panel");
	}
	
	public void switchToTax(TaxDeductionPanel panel) {
		hasCards.add(panel, "Tax Panel");
		((CardLayout) hasCards.getLayout()).show(hasCards, "Tax Panel");
	}
	
	public void makeTopBarPanel(Account tAccount, ProgramClientListener pro) {
		topBarPanel = new TopBarPanel(pro, tAccount, this);
		passToTopBar(this);
		getTopBarPanel().setAccount(tAccount);
	}
	
	public void setAccount(Account account) {
		this.account = account;
	}
	
	public Account getAccount() { return account; }
	
	public TopBarPanel getTopBarPanel() {
		return topBarPanel;
	}

	public NotLoggedInTopBarPanel getNotLoggedInTopBarPanel() {
		return notLoggedInTopBarPanel;
	}
	
	public Feed getFeed() { return feed; }
	
	public JPanel getCards() {
		return hasCards;
	}
	
}
