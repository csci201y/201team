package client;


import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.border.LineBorder;

public class ProfilePanel extends JPanel {

	private static final long serialVersionUID = 1L;
	private String accountType;
	private Account account;
	private int profilePicture;
	private String picturePath;
	

	public ProfilePanel(Account account) {
		super();
		accountType = account.getAccountType();
		profilePicture = account.getProfilePicture();
		picturePath = getImage(profilePicture);
		this.account = account;
		instantiateComponents();
		createGUI();
		addActions();
		this.setVisible(true);
	}

	private void instantiateComponents() {
		
		setPreferredSize(new Dimension(Constants.FEED_PANEL_WIDTH, Constants.FEED_PANEL_HEIGHT));
		
		JPanel mainPanel = new JPanel();
		mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.X_AXIS));
		
		JPanel profilePanel = new JPanel();
		ImageIcon profilepic = new ImageIcon(picturePath);
		JLabel profilepicLabel = new JLabel(profilepic);
		profilePanel.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		
		c.gridx = 1;
		c.gridy = 0;
		c.gridwidth = 1;

		c.insets = new Insets(5, 5, 5, 5);
		profilePanel.add(profilepicLabel, c);
		
		JLabel nameLabel = new JLabel(account.getUsername());
		nameLabel.setFont(new Font(null, Font.BOLD, 16));
		
		c.gridx = 1;
		c.gridy = 2;
		c.gridwidth = 1;
		profilePanel.add(nameLabel, c);
		
		JLabel emailLabel = new JLabel(account.getContact());
		c.gridx = 1;
		c.gridy = 3;
		c.gridwidth = 1;
		profilePanel.add(emailLabel, c);
		
		JLabel accountTypeLabel = new JLabel(accountType);
		c.gridx = 1;
		c.gridy = 4;
		c.gridwidth = 1;
		profilePanel.add(accountTypeLabel, c);
		
		JPanel descriptionPanel = new JPanel();
		descriptionPanel.setLayout(new GridBagLayout());
		GridBagConstraints c2 = new GridBagConstraints();
		
		JTextArea descArea = new JTextArea(5, 35);
		descArea.setLineWrap(true);
		descArea.setBorder(new LineBorder(Color.BLACK));
		System.out.println("ProP desc: " + account.getDescription());
		descArea.setText(account.getDescription());
		descArea.setEditable(false);
		
		JLabel descLabel = new JLabel("Description: ");
		
		c2.gridx = 0;
		c2.gridy = 0;
		c2.gridwidth = 1;
		c2.insets = new Insets(2, 2, 2, 5);
		descriptionPanel.add(descLabel, c2);
		
		c2.gridx = 0;
		c2.gridy = 1;
		c2.gridwidth = 4;
		descriptionPanel.add(descArea, c2);
		
		
		mainPanel.add(profilePanel);
		mainPanel.add(Box.createRigidArea(new Dimension(25 ,25)));
		mainPanel.add(descriptionPanel);
		add(mainPanel);
	}

	private void createGUI() {
		setBorder(new LineBorder(Color.WHITE));

	}
	
	private void addActions() {
		
	}
	
	public static String getImage(int pp) {
		String path = null;
		if (pp == 1) {
			path = "src/resources/img/male1PP.png";
		} else if (pp == 2) {
			path = "src/resources/img/female1PP.png";
		} else if (pp == 3) {
			path = "src/resources/img/male2PP.png";
		} else if (pp == 4) {
			path = "src/resources/img/female2PP.png";
		} else if (pp == 5) { 
			path = "src/resources/img/pawPP.png";
		} else if (pp == 6) {
			path = "src/resources/img/spaceshuttlePP.png";
		} else if (pp == 7) {
			path = "src/resources/img/flowerPP.png";
		} else if (pp == 8) {
			path = "src/resources/img/computerPP.png";
		} else if (pp == 9) {
			path = "src/resources/img/robotPP.png";
		} else if (pp == 10) {
			path = "src/resources/img/heartPP.png";
		} else if (pp == 11) {
			path = "src/resources/img/applePP.png";
		} else if (pp == 12) {
			path = "src/resources/img/tractorPP.png";
		} 
		
		return path;
	}
	
}
