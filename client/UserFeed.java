package client;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneLayout;

import message.UpdateUserFeedMessage;

public class UserFeed extends JPanel {

	public static final long serialVersionUID = 1L;
	private static final boolean DEBUGGING = false;
	private static final boolean COLOR_GRAPHICS = false;
	
	private final int AMOUNT_TO_DISPLAY = 5;
	
	private Account account;
	private String username;

	private Vector<Account> accounts;
	private Vector<String> usernames;
	private ArrayList< FeedInfoItem<Account> > infoToDisplay;
	
	private JPanel infoPanel;
	private JScrollPane scrollBarPanel;
	private MakePostButton makePostButton;
	
	private ProgramClientListener pcl;
	private boolean waitingForPosts;
	
	public UserFeed(Account a, ProgramClientListener pcl) {
		account = a;
		accounts = new Vector<Account>();
		accounts.add(a);
		
		username = a.getUsername();
		usernames = new Vector<String>();
		usernames.add(username);
		
		this.pcl = pcl;
		
		instantiateVariables();	
		
		updateUserFeed();
	}
	
	private void instantiateVariables() {
		waitingForPosts = false;
		
		infoToDisplay = new ArrayList<FeedInfoItem<Account>>();
		// infoToDisplay - set up blank templates / placeholders
		for ( int i = 0; i < AMOUNT_TO_DISPLAY; i++ ) {
			infoToDisplay.add(new FeedInfoItem<Account>());
			infoToDisplay.get(i).setAccount(account);
			infoToDisplay.get(i).setUsername(username); //set the info's username to the logged in user
		}
		
		// makePostButton
		makePostButton = new MakePostButton(username);
		makePostButton.setProgramClientListener(pcl);
		// infoPanel
		infoPanel = new JPanel();
		infoPanel.setLayout(new BoxLayout(infoPanel,BoxLayout.PAGE_AXIS));
		infoPanel.add(Box.createHorizontalStrut(10));
		infoPanel.add(makePostButton);
		infoPanel.add(Box.createGlue());
		for (FeedInfoItem<Account> info : infoToDisplay) {
			infoPanel.add(info);
		}
		// scrollBarPanel
		scrollBarPanel = new JScrollPane();
		//scrollBarPanel.setPreferredSize(new Dimension(Constants.FEED_PANEL_WIDTH, Constants.FEED_PANEL_HEIGHT));
		scrollBarPanel.setLayout(new ScrollPaneLayout());
		scrollBarPanel.setViewportView(infoPanel);
		scrollBarPanel.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		// this panel
		if (COLOR_GRAPHICS) this.setBackground(Color.GREEN);
		
		this.setPreferredSize(new Dimension(40,50));
		//this.setPreferredSize(new Dimension(Constants.FEED_PANEL_WIDTH, Constants.FEED_PANEL_HEIGHT));

		infoPanel.setVisible(true);
		scrollBarPanel.setVisible(true);

		this.setLayout(new BorderLayout());
		this.add(scrollBarPanel, BorderLayout.CENTER);
		this.setVisible(true);
	}
	
	public void updateUserFeed(){
		waitingForPosts = true;
		pcl.sendMessage(new UpdateUserFeedMessage(accounts, AMOUNT_TO_DISPLAY));
	}

	// receives the returned UpdateUserFeedMessage
	public void updateUserFeed(UpdateUserFeedMessage vm) {
		if ( waitingForPosts ) {
			waitingForPosts = false;
			
			if (vm.getAmountOfPosts() != AMOUNT_TO_DISPLAY) {
				if(DEBUGGING) {
					System.out.print("WARNING: amount of messages returned doesn't match up. "); 
					System.out.print("Expected " + AMOUNT_TO_DISPLAY);
					System.out.println(", received " + vm.getAmountOfPosts());
				}
			}
			
			Vector<Account> acc = vm.getAccounts();
			for (int j = 0; j < acc.size(); j++) {
				if (vm.getAccount(j) != null && vm.getAmount(j) != null) {
					String[] post = vm.getPost(j);
					if (!post[0].equals(username)) continue;
					infoToDisplay.get(j).setMessage(post[1]);
					infoToDisplay.get(j).setMaterial(post[2]);
					infoToDisplay.get(j).setAmount(Integer.parseInt(post[3]));
				} else {
					infoToDisplay.get(j).reset();
					infoToDisplay.get(j).setVisible(false);
				}
			}
			for (int k = acc.size(); k < infoToDisplay.size(); k++) {
				infoToDisplay.get(k).reset();
				infoToDisplay.get(k).setVisible(false);
			}
		}
	}
	
	public Account getAccount() { return account; }
	
	public void setAccount(Account a) {
		account = a;
		accounts.clear();
		accounts.addElement(a);
		updateUserFeed();
	}
	
	public String getUsername() { return username; }
	
	public void setUsername(String username) { this.username = username; }
}
