package client;

import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextField;

import message.SearchMessage;

public class NotLoggedInTopBarPanel extends JPanel {
	
	private static final long serialVersionUID = 1L;
	private JTextField searchField;
	private JButton loginButton, signupButton, searchButton, homeButton;

	private LoginGUI lgui;
	private SignUpGUI sug;
	private ProgramClientListener pcl;
	private MainGUI main;
	
	public NotLoggedInTopBarPanel(ProgramClientListener pcl, MainGUI main) {
		this.pcl = pcl;
		this.lgui = new LoginGUI(pcl);
		this.sug = new SignUpGUI(null, pcl);
		this.main = main;
		if (this.pcl == null) {
			System.out.println("pcl is null in NLITBP");
		}
		instantiateComponents();
		createGUI();
		addActions();
	}

	private void instantiateComponents() {
		ImageIcon logo = new ImageIcon("src/resources/img/logo.png");
		homeButton = new JButton("", logo);
		homeButton.setOpaque(false);
		homeButton.setFocusPainted(false);
		homeButton.setBorderPainted(false);
		homeButton.setContentAreaFilled(false);
			
		searchField = new JTextField(100);
		searchField.setPreferredSize(new Dimension(300, 25));
		searchField.setMaximumSize(searchField.getPreferredSize());
		searchButton = new JButton("Search");
		
		ImageIcon loginImage = new ImageIcon("src/resources/img/login.png");
		loginButton = new JButton("", loginImage);
		loginButton.setOpaque(false);
		loginButton.setFocusPainted(false);
		loginButton.setBorderPainted(false);
		loginButton.setContentAreaFilled(false);
		
		ImageIcon signupImage = new ImageIcon("src/resources/img/signup.png");
		signupButton = new JButton("", signupImage);	
		signupButton.setOpaque(false);
		signupButton.setFocusPainted(false);
		signupButton.setBorderPainted(false);
		signupButton.setContentAreaFilled(false);
	}

	private void createGUI() {
		setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
		
		add(Box.createRigidArea(new Dimension(10, 0)));
		add(homeButton);
		add(Box.createRigidArea(new Dimension(170, 0)));
		add(searchField);
		add(searchButton);
		add(Box.createRigidArea(new Dimension(150, 0)));
		add(loginButton);
		add(signupButton);
		add(Box.createRigidArea(new Dimension(10, 0)));
		
		setBackground(new Color(35, 81, 127));
		
	}

	private void addActions() {
		
		homeButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				main.getFeed().updateFeed();
				((CardLayout) main.getCards().getLayout()).show(main.getCards(), "Feed");
			}
			
		});
		
		searchButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				System.out.println(searchField.getText());
				SearchMessage mess = new SearchMessage(searchField.getText());
				pcl.sendMessage(mess);
			}
			
		});
		
		loginButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				lgui.setVisible(true);
			}
			
		});
		
		signupButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				sug.setVisible(true);
			}
			
		});
	}
	
	public LoginGUI getLoginGUI() {
		return lgui;
	}
	
	public SignUpGUI getSignUpGUI() {
		return sug;
	}

}
