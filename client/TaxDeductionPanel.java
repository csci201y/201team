package client;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class TaxDeductionPanel extends JPanel {

	private static final long serialVersionUID = 1L;
	private JLabel donationLabel, taxRateLabel, netCostLabel, taxSavingsLabel;
	private JTextField donation, netCost, taxSavings;
	private JComboBox<Double> taxRate;
	private JLabel taxBrackets;
	private JButton calculate;
	private JPanel interactive;
		
	TaxDeductionPanel()
	{
		this.setSize(720, 400);
		this.setBackground(new Color(238, 238, 238));
		this.setVisible(true);
		
		instantiateVariables();
		setUpGUI();
		addActionListeners();
	}
	
	public void instantiateVariables()
	{
		donationLabel = new JLabel("Donation ($)");
		taxRateLabel = new JLabel("Tax Rate (%)");
		netCostLabel = new JLabel("Net Cost of Donation ");
		taxSavingsLabel = new JLabel("Tax Savings");
		donation = new JTextField(10);
		netCost = new JTextField(10);
		netCost.setEditable(false);
		taxSavings = new JTextField(10);
		taxSavings.setEditable(false);
		calculate = new JButton("Calculate");
		
		Double [] rates = new Double[] {10., 15., 25., 28., 33., 35., 39.6};
		taxRate = new JComboBox<Double>(rates);
		
		Image brackets = Toolkit.getDefaultToolkit().getImage(getClass().getResource("/resources/img/tax_bracket_info.png"));
		ImageIcon bracketsIcon = new ImageIcon(brackets);
		taxBrackets = new JLabel(bracketsIcon);
	}
	
	public void setUpGUI()
	{
		interactive = new JPanel();
		interactive.setLayout(new GridLayout(0, 4));
		//row 1
		interactive.add(donationLabel);
		interactive.add(taxRateLabel);
		interactive.add(netCostLabel);
		interactive.add(taxSavingsLabel);
		//row 2
		interactive.add(donation);
		interactive.add(taxRate);
		interactive.add(netCost);
		interactive.add(taxSavings);	 
		//row 3
		interactive.add(calculate);
		
		add(interactive, BorderLayout.CENTER);
		add(taxBrackets, BorderLayout.SOUTH);

		repaint();
		revalidate();
	}
	
	public void addActionListeners()
	{
		calculate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) 
			{
				double donateValue = Double.parseDouble(donation.getText());
				double taxSavingsValue = donateValue * (((double) taxRate.getSelectedItem()) / 100.);
				double netCostValue = donateValue - taxSavingsValue;
				
				netCost.setText(String.valueOf(netCostValue));
				taxSavings.setText(String.valueOf(taxSavingsValue));
				
				repaint();
				revalidate();
			}
		});
	}

}
