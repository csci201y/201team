package client;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ListSelectionModel;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import message.AcceptMessage;
import message.ChatMessage;
import message.CleanMessage;

public class ChatFrame extends JFrame{ //contain the GUI of chat 
	
	private String user;
	private ChatThread ct;
	
	private JPanel DisplayPane;
	private JButton SendButton; //send the request 
	private JLabel RequestLabel;
	
	//private String [] HasChatname = {"Emma", "Serena"};//get those two list of username from ChatThread
	private JLabel Rlabel;
	private JLabel Slaebl;
	
	
	private ArrayList<String> RequestChatname;
	private JList Haslist;
	
	//private String [] HasnotChatname = {"Duncun", "Chris"};
	
	private String mSelection;
	 
	private Vector<String> Requestname;
	private JTextArea Reminder;
	 
	private JPanel temp;
	
    // public ChatFrame(ChatThread ct, String user){
	public ChatFrame(ChatThread ct, String user){
    	 super();
 		 setTitle("Chat");
 		
 		this.user = user;
 		setMinimumSize(new Dimension(500,400));
		setLocationRelativeTo(null);
		//setDefaultCloseOperation(EXIT_ON_CLOSE);
		
    	 this.ct = ct;
    	 //HasChatname = {"Emma", "Serena"};//Will get from ct
    	 //HasnotChatname = {"Duncun", "Chris"};
    	 while(ct.getTest() == false){
    		 System.out.println("were");
    	 }
    	 ChatGUI();
    	 sendrequest();
    	 
    	 setVisible(true); 
     }
     
     public void ChatGUI(){
    	  
    	 DisplayPane = new JPanel();
    	 DisplayPane.setLayout(new BoxLayout(DisplayPane, BoxLayout.Y_AXIS));
    	 
    	 ImageIcon logo = new ImageIcon("src/resources/img/logo.png");
    	 JLabel logoLabel = new JLabel(logo);
    	 JPanel topPanel = new JPanel();
    	 topPanel.setLayout(new BoxLayout(topPanel, BoxLayout.X_AXIS));
    	 topPanel.add(Box.createRigidArea(new Dimension(80,0)));
    	 topPanel.add(logoLabel);
    	 topPanel.add(Box.createRigidArea(new Dimension(80,0)));
    	 
    	 SendButton = new JButton("Invite Someone to Chat");
    	 JPanel buttonPanel = new JPanel();
    	 buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.X_AXIS));
    	 buttonPanel.add(Box.createRigidArea(new Dimension(80, 0)));
    	 buttonPanel.add(SendButton);
    	 buttonPanel.add(Box.createRigidArea(new Dimension(80, 0)));
    	 
    	 JPanel temp = new JPanel();
    	 temp.setLayout(new BoxLayout(temp, BoxLayout.X_AXIS));
    	 
    	 ImageIcon chatImage = new ImageIcon("src/resources/img/chatrequire.png");
 		 JLabel Chatinfo = new JLabel(chatImage);
    	 RequestLabel = new JLabel("Recent Chat Info");
    	 temp.add(Box.createRigidArea(new Dimension(72, 0)));
 		 temp.add(Chatinfo);
    	 temp.add(Box.createRigidArea(new Dimension(6, 0)));
 		 temp.add(RequestLabel);
 		 temp.add(Box.createRigidArea(new Dimension(72, 0)));
     	
    	 Rlabel =new JLabel("Chat Requests:");
    	 JPanel requestsPanel = new JPanel();
    	 requestsPanel.setLayout(new BoxLayout(requestsPanel, BoxLayout.X_AXIS));
    	 requestsPanel.add(Box.createRigidArea(new Dimension(80, 0)));
    	 requestsPanel.add(Rlabel);
    	 requestsPanel.add(Box.createRigidArea(new Dimension(80, 0)));
    	
    	 
    	 RequestChatname = ct.getRequestChatname();
     
    	 if(RequestChatname !=null){
    	 Haslist = new JList(RequestChatname.toArray());
    	 Haslist.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
		 Haslist.setLayoutOrientation(JList.HORIZONTAL_WRAP);
		 Haslist.setVisibleRowCount(-1);
		 JScrollPane listScroller = new JScrollPane(Haslist);
		 Haslist.setBorder(new EmptyBorder(10,10, 10, 10));
		 listScroller.setPreferredSize(new Dimension(250, 150));
		   
		 ListSelectionModel listSelectionModel = Haslist.getSelectionModel();
		 listSelectionModel.addListSelectionListener(
		                            new SharedListSelectionHandler());
		  
		 DisplayPane.add(topPanel);
		 DisplayPane.add(buttonPanel);
		 DisplayPane.add(temp);
		 DisplayPane.add(requestsPanel);
		 DisplayPane.add(listScroller);

    	 }
    	 else{
    		 Haslist = null;//data has type Object[]
    	 }
    	 
    	 
	     Slaebl = new JLabel("Chat Requests Sent:");
    	 JPanel sentPanel = new JPanel();
    	 sentPanel.setLayout(new BoxLayout(sentPanel, BoxLayout.X_AXIS));
    	 sentPanel.add(Box.createRigidArea(new Dimension(80, 0)));
    	 sentPanel.add(Slaebl);
    	 sentPanel.add(Box.createRigidArea(new Dimension(80, 0)));
    	 
	     DisplayPane.add(sentPanel);
		 Reminder = new JTextArea();
		 Reminder.setEditable(false);
		 DisplayPane.add(Reminder);
		 getContentPane().add(DisplayPane);
     }
     
     public void sendrequest(){
    	 SendButton.addActionListener(new ActionListener() {

 			@SuppressWarnings("deprecation")
 			@Override
 			public void actionPerformed(ActionEvent e) {
 				//RequestChatMessage rc = new RequestChatMessage(user);
 				//String [] allname = tc.getAllname();
 			 ArrayList<String> file = ct.getAllname();//get all the name 
 			 StartChatInvitation sc = new StartChatInvitation(user,ChatFrame.this,file, ct);
 			}

 			});
     }
     
     private class SharedListSelectionHandler implements ListSelectionListener { //private class for list listener 
			public void valueChanged(ListSelectionEvent e) {
			    if (e.getValueIsAdjusting() == false) {

			        if (Haslist.getSelectedIndex() == -1) {
		               System.out.println("Nothing is selected");
			        } else {
			           System.out.println("In our Haslist: " + Haslist.getSelectedValue());
			           mSelection = (String)Haslist.getSelectedValue();
			           AcceptChatInvitation(mSelection,0);
			           try {
		   					Thread.sleep(100);
		   				} catch (InterruptedException e1) {
		   					// TODO Auto-generated catch block
		   					e1.printStackTrace();
		   				}
			           
			        }
			    }
			}
			}
    
     
     public void setRemind(String name){
    	 Reminder.append(name + "\n");
     }
     
     private void AcceptChatInvitation(String m, int listype){
    	//String 
 		int selection = JOptionPane.showOptionDialog(null, "Do you want to accept the chat request from " + m, "Chat Request...",JOptionPane.OK_CANCEL_OPTION,JOptionPane.INFORMATION_MESSAGE, null, new String[]{"Agree" ,"No"}, "default");
 		System.out.println("Accept the suggesetion");
 		
 		if(selection == JOptionPane.OK_OPTION){
 	    	String tt=null;
 	    	AcceptMessage cm = new AcceptMessage(m, user);
 	    	System.out.println("Sending accept ");
 	    	ct.sendAcceptMessage(cm); //tell the user we start to chat 
 	    	//ChatHistoryScrollPane CHS = new ChatHistoryScrollPane(m, user, tt, ct);	
 	    	System.out.println("Set Caller: " + m + "Set user: " + user);
 	    	ct.getCSP().setCaller(m);
 	    	ct.getCSP().setUser(user);
 	    	ct.getCSP().updateJLabel(m);
 	    	ct.getCSP().setVisible(true);
 	    	
 	    	CleanMessage cn = new CleanMessage(m ,user);
 	    	System.out.println("After accpet, I am sending clean" );
 	    	ct.sendCleanMessage(cn);
 	    }else{
 	    	CleanMessage cm = new CleanMessage(m ,user);
 	    	System.out.println("I am sending" );
 	    	ct.sendCleanMessage(cm);
 	    }
 	    
 	     for(int g=0; g< RequestChatname.size(); g++){
 	    	 if(RequestChatname.get(g) == m){
 	    		 RequestChatname.remove(g);
 	    		 break;
 	    	 }
 	     }
 	     System.out.println(RequestChatname.size());
 	     Haslist.setListData(RequestChatname.toArray());
 	     Haslist.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
		 Haslist.setLayoutOrientation(JList.HORIZONTAL_WRAP);
		 Haslist.setVisibleRowCount(-1);
		 Haslist.setBorder(new EmptyBorder(10,10, 10, 10));
		 
		 Haslist.revalidate();
		 Haslist.repaint(); 
 	    
 	   /* if(selection == JOptionPane.CANCEL_OPTION){
 	    	if(listype == 0){
 	    		for(int y=0; y< HasChatname.length; y++){
 	    			if(HasChatname[y] == m){
 	    				while(y< HasChatname.length-1){
 	    				HasChatname[y] = HasChatname[y+1];
 	    				}
 	    			}
 	    		}
 	    		
 	          
 	    		
 	          
 	    	}
 	    	else{
 	    		for(int y=0; y< HasnotChatname.length; y++){
 	    			if(HasnotChatname[y] == m){
 	    				while(y< HasnotChatname.length-1){
 	    				HasnotChatname[y] = HasChatname[y+1];
 	    				}
 	    			}
 	    		}
 	         
 	   		  
 	    	}
 	    } */
 	}
     
    /* public static void main(String [] args) {
 		//new ChatClient("localhost", 6789, user);
    	// String 
    	// ChatThread ct = new ChatThead(String hostname, int port);
    	// new ChatFrame(ct,user);
    	 new ChatFrame();
 	}*/
}

   