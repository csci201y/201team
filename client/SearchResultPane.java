package client;

import java.awt.*;
import javax.swing.*;

import message.SearchMessage;

public class SearchResultPane extends JPanel {
	
	public static final long serialVersionUID = 1;
	private ProfilePanel resultProfilePanel;
	private JScrollPane toScroll;
	private JPanel resultPanel;
	private ProgramClientListener s;
	private Account accountResult;
	
	SearchResultPane(String search, ProgramClientListener s)
	{
		this.s = s;
		getResult(search);
		instantiateVariables();
		createGUI();
	}
	
	SearchResultPane(Account c)
	{
		
		System.out.println("here");
		
		if(c != null)
		{
			System.out.println(c.getUsername());
			accountResult = c;
			resultProfilePanel =new ProfilePanel(accountResult);
		}
		instantiateVariables();
		createGUI();
	}

	public void getResult(String search)
	{
		SearchMessage mess = new SearchMessage(search);
		
		s.sendMessage(mess);
		/*ObjectOutputStream out = new ObjectOutputStream(s.getOutputStream());
		ObjectInputStream in = new ObjectInputStream(s.getInputStream());
		out.writeObject(mess);
		out.flush(); */

		System.out.println(accountResult.getUsername());

		if(accountResult != null)
		{
			resultProfilePanel.add(new ProfilePanel(accountResult));
		}
	}
	
	public void instantiateVariables()
	{
		resultPanel = new JPanel();
		
		toScroll = new JScrollPane();
		toScroll.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER );
		toScroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		toScroll.setPreferredSize(new Dimension(Constants.FEED_PANEL_WIDTH,Constants.FEED_PANEL_HEIGHT));
    	toScroll.setViewportView(resultPanel);
	}
	
	public void createGUI()
	{
		if(accountResult != null)  {
			resultPanel.add(resultProfilePanel);
		} else if(accountResult == null) {
				resultPanel.add(new JLabel("No results were found for your search."));
			}
		
		add(resultPanel);
		repaint();
		revalidate();
		setVisible(true);
	}
	
	
}
