package client;

public class VisitorAccount extends Account {
	
	
	/*** default */
	private static final long serialVersionUID = 1L;

	VisitorAccount(String user, String pass)
	{
		super(user, pass);
		super.setCanChat(false);
		super.setCanDonate(false);
		super.setCanReceiveDonations(false);
	}

}
