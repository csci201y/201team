package client;

public class CharityAccount extends Account {
	
	/*** default */
	private static final long serialVersionUID = 1L;

	CharityAccount(String name, String pass) 
	{
		super(name, pass);
		super.setCanChat(true);
		super.setCanReceiveDonations(true);
		super.setCanDonate(false);
	}


}
