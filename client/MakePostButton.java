package client;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;

import message.UpdateFeedMessage;


//textfield single line
//button moves (bc glue)

/*
 * updates its materials list from the server
 * when the button is clicked, pops up / hides (toggles visibility of) a JDialog prompting post info.
 */
public class MakePostButton extends JButton {

	/*** default */
	private static final long serialVersionUID = 1L;
	public static final boolean DEBUGGING = Constants.DEBUGGING_FEED;
	
	private String username;
	private MakePostDialog makePostDialog;
	
	private ProgramClientListener pcl;
	
	public MakePostButton(String username) {
		super("Make Post");
		this.username = username;
		addActionListeners();
	}
	
	public MakePostButton(ImageIcon image, String username) {
		super("", image);
		this.username = username;
		addActionListeners();
	}
	
	public void setUsername(String u) { username = u; }
	
	public void setProgramClientListener(ProgramClientListener pcl) { this.pcl = pcl; }
	
	private void addActionListeners() {
		
		Vector<String> materials = new Vector<String>(); //TODO
		for (int i = 0; i < Constants.MATERIALS.length; i++ ) {
			materials.add(Constants.MATERIALS[i]);
		}
		makePostDialog = new MakePostDialog(materials);
		
		this.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				makePostDialog.setVisible(!makePostDialog.isVisible());
			}
		});
	}

	private class MakePostDialog extends JDialog {
		/*** default */
		private static final long serialVersionUID = 1L;
		
		private static final int MAX_AMOUNT = 100;
		private MakePostDialog mpd; //refernce to itself
		
		public MakePostDialog(Vector<String> materials) {
			super();
		
			mpd = this;
			
			JPanel panel, panel2, descrPanel, matsPanel, amtsPanel;
			JLabel descrErrorLabel, matsErrorLabel, amtsErrorLabel;
			JLabel descrLabel, matsLabel, amtsLabel;
			JTextArea description;
			JComboBox<String> materialComboBox;
			Vector<Integer> amts;
			JComboBox<Integer> amountComboBox;
			JButton makePost;
			
			this.setTitle("Make Post");
			this.setLocationRelativeTo(this.getParent());
			
			panel = new JPanel();
			panel2 = new JPanel();
			descrPanel = new JPanel();
			matsPanel = new JPanel();
			amtsPanel = new JPanel();
			panel.setLayout(new BoxLayout(panel,BoxLayout.Y_AXIS));
			panel2.setLayout(new BoxLayout(panel2,BoxLayout.Y_AXIS));
			descrPanel.setLayout(new BoxLayout(descrPanel, BoxLayout.X_AXIS));
			matsPanel.setLayout(new BoxLayout(matsPanel, BoxLayout.X_AXIS));
			amtsPanel.setLayout(new BoxLayout(amtsPanel, BoxLayout.X_AXIS));
			
			this.setSize(new Dimension(250,250));
			this.setVisible(false);
			this.setDefaultCloseOperation(JDialog.HIDE_ON_CLOSE);
			
			descrLabel = new JLabel("Description");
			matsLabel = new JLabel("Material");
			amtsLabel = new JLabel("Amount");
			descrErrorLabel = new JLabel("");
			matsErrorLabel = new JLabel("");
			amtsErrorLabel = new JLabel("");
			
			description = new JTextArea();
			description.setText("Description here...");
			
			materialComboBox = new JComboBox<String>(materials);
			
			amts = new Vector<Integer>();
			for (int i = 1; i <= MAX_AMOUNT; i++) {
				amts.add(new Integer(i));
			}
			amountComboBox = new JComboBox<Integer>(amts);
			
			makePost = new JButton("Post");
			
			descrPanel.add(descrLabel);
			descrPanel.add(Box.createRigidArea(new Dimension(10,0)));
			descrPanel.add(descrErrorLabel);
			descrPanel.add(Box.createHorizontalGlue());
			
			matsPanel.add(matsLabel);
			matsPanel.add(Box.createRigidArea(new Dimension(10,0)));
			matsPanel.add(matsErrorLabel);
			matsPanel.add(Box.createHorizontalGlue());
			
			amtsPanel.add(amtsLabel);
			amtsPanel.add(Box.createRigidArea(new Dimension(10,0)));
			amtsPanel.add(amtsErrorLabel);
			amtsPanel.add(Box.createHorizontalGlue());
			
			panel2.add(Box.createHorizontalGlue());
			panel2.add(makePost);
			
			panel.add(descrPanel);
			panel.add(description);
			panel.add(matsPanel);
			panel.add(materialComboBox);
			panel.add(amtsPanel);
			panel.add(amountComboBox);
			panel.add(panel2);
			this.add(panel);
			
			makePost.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					//send post information to server
					String mat; //material
					int amt; 	//amount of material
					
					if (description.getText().equals("")) { descrErrorLabel.setText("Warning: no description set."); }
					if (materialComboBox.getSelectedIndex() != -1) { mat = materialComboBox.getItemAt(materialComboBox.getSelectedIndex());
					} else {
						matsErrorLabel.setText("Error: you must choose a material.");
						mat = "404";
					}
					if (amountComboBox.getSelectedIndex() != -1 ) { amt = amountComboBox.getItemAt(amountComboBox.getSelectedIndex()); }
					else {
						amtsErrorLabel.setText("Error: you must choose an amount.");
						amt = -404;
					}
					
					if ( mat != "404" && amt != -404) {
						UpdateFeedMessage message = new UpdateFeedMessage(username, description.getText(), mat, amt);
						if (pcl != null) {
								pcl.sendMessage(message);
								if (DEBUGGING){
									System.out.println("Sent message from " + username + ": " + 
											description.getText() + "\n" + amt + " of " + mat);
								}		
								descrErrorLabel.setText("");
								matsErrorLabel.setText("");
								amtsErrorLabel.setText("");
						} else {
							String warning_message = "Warning: cannot connect to server. ";
							if (DEBUGGING) warning_message += "MakePostButton's ProgramClientListener is null.";
							JOptionPane.showMessageDialog(makePost, warning_message, "warning", JOptionPane.ERROR_MESSAGE);
						}
						mpd.setVisible(false);
					}
				}
			});
		}
		
	}
}