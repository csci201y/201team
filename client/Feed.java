package client;
//package name

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneLayout;

import message.DefaultFeedMessage;
import message.SendFavoritesMessage;
import message.UpdateUserFeedMessage;

// TODO: if time, add a delete post button/option
// TODO: if time, [post completed] label or something

// ACCOUNT TYPES: CharityAccount, IndividualAccount, BusinessAccount, Account
// FEED TYPES: GiverFeed, ReceiverFeed, UserFeed, NotLoggedInFeed
// INFO TYPES: GiverInfo, Info, CharityInfo

// GiverFeed displays CharityInfo for a list of CharityAccounts
// ReceiverFeed displays GiverInfo and for a list of IndividualAccount / BusinessAccount
// NotLoggedInFeed displays Info for a list of Accounts
// UserFeed displays the Info associated with your Account

public class Feed extends JPanel implements Runnable {

	/** auto-generated */
	private static final long serialVersionUID = 1L;
	public static final boolean DEBUGGING = Constants.DEBUGGING_FEED;

	private static final int MESSAGES_PER_ACCOUNT = 2;
	private static final int NUM_ACCOUNTS = 6;
	private static final int UPDATE_INTERVAL_MILLIS = 5000;

	// data
	private Account account;
	private Vector<Account> accountsToDisplay;
	private Vector<FeedInfoItem<Account>> infoToDisplay;

	// GUI
	public final int PANEL_WIDTH, PANEL_HEIGHT;

	private JScrollPane scrollPane;
	private JPanel infoPane;
	private MakePostButton makePostButton;
	private JButton refreshButton;

	private ProgramClientListener pcl;
	
	private boolean waitingForDefaults;
	private boolean waitingForPosts;
	
	// *** constructor ***//

	public Feed(Account loggedInAccount, ProgramClientListener pcl) {
		PANEL_WIDTH = Constants.FEED_PANEL_WIDTH;
		PANEL_HEIGHT = Constants.FEED_PANEL_HEIGHT;

		this.account = loggedInAccount;
		this.pcl = pcl;

		instantiateVariables();
		createGUI();

		updateFeed();

		this.setVisible(true);
		
		//new Thread(this).start();
	}

	private void instantiateVariables() {

		waitingForDefaults = false;
		waitingForPosts = false;

		if (account == null) {
			if (DEBUGGING)
				System.out.println("ERROR: Cannot create Feed for a null account.");
		}

		accountsToDisplay = new Vector<Account>();
		infoToDisplay = new Vector<FeedInfoItem<Account>>();

		synchronized (account) {
			if (account instanceof VisitorAccount) {
				if (DEBUGGING)
					System.out.println("Account type: visitor");
				accountsToDisplay = account.getDefaultFeedAccounts();
			} else if (account instanceof BusinessAccount || account instanceof IndividualAccount
					|| account instanceof CharityAccount) {
				if (DEBUGGING)
					System.out.println("Account type: business, individual, or charity");
				accountsToDisplay = account.getFavoriteAccounts();
			} else { // account instanceof Account
				if (DEBUGGING)
					System.out.println("Account type: default Account");
				if (DEBUGGING)
					System.out.println("WARNING: '" + account.getUsername() + "' is of type 'default Account'.");
				accountsToDisplay = account.getDefaultFeedAccounts();
			}
		}

		infoPane = new JPanel();
		infoPane.setLayout(new BoxLayout(infoPane, BoxLayout.PAGE_AXIS));
		scrollPane = new JScrollPane();
		scrollPane.setLayout(new ScrollPaneLayout());
		scrollPane.setViewportView(infoPane);
		scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);

		makePostButton = new MakePostButton(account.getUsername());
		makePostButton.setProgramClientListener(pcl);

		refreshButton = new JButton("Refresh");
		refreshButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				updateFeed();
			}
		});
	}

	private void createGUI() {
		if (DEBUGGING)
			scrollPane.setBackground(Color.green);
		if (DEBUGGING)
			infoPane.setBackground(Color.lightGray);
		if (DEBUGGING)
			this.setBackground(Color.black);
		if (!DEBUGGING)
			infoPane.setOpaque(false);

		if (account instanceof VisitorAccount) {
			makePostButton.setEnabled(false);
		} else if (account instanceof IndividualAccount || account instanceof BusinessAccount
				|| account instanceof CharityAccount) {
			makePostButton.setEnabled(true);
		} else {
			makePostButton.setEnabled(true);
		}

		//infoPane.add(Box.createGlue());
		//infoPane.add(refreshButton);
		//infoPane.add(Box.createHorizontalStrut(10));
		//infoPane.add(makePostButton);
		//infoPane.add(Box.createGlue());

		populateInfoToDisplay();
		addPanels();

		infoPane.setVisible(true);
		scrollPane.setVisible(true);

		this.setLayout(new BorderLayout());
		this.add(scrollPane, BorderLayout.CENTER);
		this.setVisible(true);
	}

	public void run() {
		while(true){
			try{
				Thread.sleep(UPDATE_INTERVAL_MILLIS);
				this.updateFeed();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	// GUI/HELPER: a separate method in case we want to be able to manually call
	// this, eg from a buton
	public void updateFeed() {

		if (DEBUGGING)
			System.out.println("Requesting to update the feed of " + account.getUsername());

		if (account instanceof VisitorAccount || account.getUsername().equals("username")) {
			makePostButton.setEnabled(false);
		} else if (account instanceof IndividualAccount || account instanceof BusinessAccount
				|| account instanceof CharityAccount) {
			makePostButton.setEnabled(true);
		} else {
			makePostButton.setEnabled(true);
		}

		// update accounts to show
		accountsToDisplay = null;
		accountsToDisplay = new Vector<Account>();

		// get favorites
		pcl.sendMessage(new SendFavoritesMessage(new Vector<Account>()));
	}
	
	// receives favorites list and requests list of default feeds if there
	// aren't enough fav's
	public void receiveFavorites(SendFavoritesMessage message) {
		accountsToDisplay = message.getFavorites();

		if (DEBUGGING) {
			System.out.println("received favorites: " + message.getFavorites().size());
			for (int i = 0; i < message.getFavorites().size(); i++) {
				System.out.println("\t" + message.getFavorites().get(i).getUsername());
			}
		}

		if (message.getFavorites().size() < NUM_ACCOUNTS) {
			waitingForDefaults = true;
			pcl.sendMessage(new DefaultFeedMessage(null, true));
		} else {
			// send current list to server
			waitingForPosts = true;
			pcl.sendMessage(new UpdateUserFeedMessage(message.getFavorites(), MESSAGES_PER_ACCOUNT));
		}
	}

	// called by ProgramClientListener when received the list of default
	// accounts. TODO Hopefully there won't be multithreading issues here.
	public void updateDefaults(DefaultFeedMessage message) {
		if (waitingForDefaults) {
			waitingForDefaults = false;

			if (DEBUGGING) {
				System.out.println("received defaults: " + message.getAccounts().size());
				for (int i = 0; i < message.getAccounts().size(); i++) {
					System.out.print("\t" + message.getAccounts().get(i).getUsername());
				}
				System.out.println("");
				System.out.println("Current number of accounts: " + accountsToDisplay.size());
				System.out.println("adding up to " + NUM_ACCOUNTS + " defaults. ");
			}

			int index;
			String curr_username, username_to_add;
			Vector<Account> accountsToAdd = new Vector<Account>();
			for (int num_accs_left = accountsToDisplay.size(); num_accs_left < NUM_ACCOUNTS; num_accs_left++) {
				index = num_accs_left - accountsToDisplay.size();
				
				if (DEBUGGING) System.out.println("\t" + num_accs_left + " - " + accountsToDisplay.size() + " = " + index);
				
				// check if the default account from message is in accountsToDisplay yet
				if (index < message.getAccounts().size()) {
					boolean contains = false;
					username_to_add = message.getAccounts().get(index).getUsername();
					for (int curr_acc = 0; curr_acc < accountsToDisplay.size(); curr_acc++) {
						curr_username = accountsToDisplay.get(curr_acc).getUsername();
						if (username_to_add.equals(curr_username) || username_to_add.equals(account.getUsername()))
							contains = true;
					}
					if (!contains)
						accountsToAdd.add(message.getAccounts().get(index));
				}
			}
			
			//add accountsToAdd to accountsToDisplay
			for (int i = 0; i < accountsToAdd.size(); i++) {
				accountsToDisplay.add(accountsToAdd.get(i));
			}

			if (DEBUGGING) {
				System.out.println("Current accounts: ");
				for (int i = 0; i < accountsToDisplay.size(); i++)
					System.out.println("\t" + accountsToDisplay.get(i).getUsername());
			}
			waitingForPosts = true;
			pcl.sendMessage(new UpdateUserFeedMessage(accountsToDisplay, MESSAGES_PER_ACCOUNT));
		}
	}

	// called by ProgramClientListener when received the list of posts
	public void updateInfoToDisplay(UpdateUserFeedMessage vm) {
		if ( waitingForPosts ) {
			waitingForPosts = false;
			
			if (DEBUGGING) {
				System.out.println("Reply received, updating feed of " + account.getUsername());
				for (int i = 0; i < vm.getAccounts().size(); i++) {
					System.out.println(
							"\t" + vm.getUsername(i) + ": " + vm.getMessage(i) + vm.getMaterial(i) + " " + vm.getAmount(i));
				}
			}
	
			Vector<Account> acc = vm.getAccounts();
			for (int j = 0; j < acc.size(); j++) {
				if (vm.getAccount(j) != null && vm.getAmount(j) != null) {
					infoToDisplay.get(j).setAccount(vm.getAccount(j));
					infoToDisplay.get(j).setUsername(vm.getUsername(j));
					infoToDisplay.get(j).setAmount(vm.getAmount(j));
					infoToDisplay.get(j).setMessage(vm.getMessage(j));
					infoToDisplay.get(j).setMaterial(vm.getMaterial(j));
					infoToDisplay.get(j).setVisible(true);
				} else {
					infoToDisplay.get(j).reset();
					infoToDisplay.get(j).setVisible(false);
				}
			}
			for (int k = acc.size(); k < infoToDisplay.size(); k++) {
				infoToDisplay.get(k).reset();
				infoToDisplay.get(k).setVisible(false);
			}
		}
	}

	// HELPER: fills infoToDisplay with FeedIinfoItems from
	// account.getFavorite/DefaultAccounts
	private void populateInfoToDisplay() {
		for (int i = 0; i < (NUM_ACCOUNTS * MESSAGES_PER_ACCOUNT); i++) {
			infoToDisplay.add(new FeedInfoItem<Account>());
		}
	}

	// GUI: add infoToDisplay's FeedInfoItems to scrollPane
	private void addPanels() {
		infoPane.add(Box.createRigidArea(new Dimension(0, 10)));
		if (infoToDisplay != null && infoToDisplay.size() > 0) {
			if (infoToDisplay.size() < (NUM_ACCOUNTS * MESSAGES_PER_ACCOUNT)) {
				for (int i = 0; i < infoToDisplay.size(); i++) {
					addPanels_Helper(infoToDisplay.get(i));
				}
				for (int i = 0; i < (NUM_ACCOUNTS * MESSAGES_PER_ACCOUNT) - infoToDisplay.size(); i++) {
					FeedInfoItem<Account> defaultFeedItem = new FeedInfoItem<Account>(
							account.getDefaultFeedAccounts().get(i));
					addPanels_Helper(defaultFeedItem);
				}
			} else {
				for (int i = 0; i < (NUM_ACCOUNTS * MESSAGES_PER_ACCOUNT); i++) {
					addPanels_Helper(infoToDisplay.get(i));
				}
			}
		} else {
			if (DEBUGGING)
				System.out.println("ERROR: populating Feed: No info to display");
		}
		infoPane.add(Box.createRigidArea(new Dimension(0, 10)));
		infoPane.add(Box.createRigidArea(new Dimension(0, 10)));
	}

	// GUI: helper method: adds FeedInfoItem to scrollPane
	private void addPanels_Helper(FeedInfoItem<Account> item) {
		if (item != null) {
			if (DEBUGGING)
				System.out.println("adding panel " + item.getName());
			infoPane.add(item);
			infoPane.add(Box.createRigidArea(new Dimension(0, 10)));
		}
	}

	public Account getAccount(String username) {
		for (Account account : accountsToDisplay) {
			if (account.getUsername() == username) {
				return account;
			}
		}
		return null;
	}

	public void setAccount(Account account) {
		this.account = account;
		makePostButton.setUsername(account.getUsername());
		updateFeed();
	}

	public void setProgramClientListener(ProgramClientListener pcl) {
		this.pcl = pcl;
	}

	/*
	 * public static void main(String args[]) { JFrame frame = new JFrame();
	 * frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	 * frame.setSize(Constants.FEED_PANEL_WIDTH, Constants.FEED_PANEL_HEIGHT);
	 * 
	 * Account account = new Account("username", "password");
	 * 
	 * Feed feed = new Feed(account); frame.getContentPane().add(feed);
	 * frame.setVisible(true); }
	 */
}