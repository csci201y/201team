package client;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.GraphicsEnvironment;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.UIManager;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import message.ChatMessage;
import message.SenderMessage;

public class StartChatInvitation extends JDialog{
	
private static final long serialVersionUID = 1L;

private ChatFrame cf;
private String user; 
private String mSelection;
private JButton mCancelButton;
private JButton mSelectButton;
private JTextField inputTextField;

private ArrayList <String> Allname;
private JList userlist;

private JLabel select;
private JLabel filetext;
private String filecontext;
private static Font customFont;

private ChatThread ct;
private String beforemessage;
 

//public OnlineFileChooser(ChatThread ct, Sring user, String [] file){
public StartChatInvitation(String user, ChatFrame cf, ArrayList<String> Allname, ChatThread ct){
	setSize(100,100);
	setResizable(false);
	setVisible(true);
	setDefaultCloseOperation(DISPOSE_ON_CLOSE);
	   this.user = user;
	   this.cf = cf;
	   this.ct = ct;
	   this.Allname = Allname;
	   try {
			Thread.sleep(100);
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}
	   //Allname = ct.getAllname();
	  
	   userlist = new JList(Allname.toArray()); //data has type Object[]
	   userlist.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
	   userlist.setLayoutOrientation(JList.HORIZONTAL_WRAP);
	userlist.setVisibleRowCount(-1);
		  
		 JScrollPane listScroller = new JScrollPane(userlist);
		 listScroller.setPreferredSize(new Dimension(250, 100));
		   
		 ListSelectionModel listSelectionModel = userlist.getSelectionModel();
		 listSelectionModel.addListSelectionListener(
		                            new SharedListSelectionHandler());
	
	mSelection = "";
	mSelectButton = new JButton("Request");
	mCancelButton = new JButton("Cancel");
	
	select = new JLabel("Users Available: ");
	
	mSelectButton.addActionListener(new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			SenderMessage SM = new SenderMessage(user, mSelection);
			//ct.sendChatMessage();
			cf.setRemind(mSelection + " ");
			ct.sendSenderMessage(SM);
			dispose();
			
		}
	});
	
	mCancelButton.addActionListener(new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			mSelection = "";
			dispose();
		}
	});
	Box contentBox = Box.createVerticalBox();
	contentBox.add(select);
	contentBox.add(listScroller);
	
	Box buttonBox = Box.createHorizontalBox();
	buttonBox.add(mSelectButton);
	buttonBox.add(mCancelButton);
	contentBox.add(buttonBox);
	add(contentBox);
	pack();
}

public String select() {
	mSelection = "";
	setLocationRelativeTo(null);
	setModal(true);
	setVisible(true);
	return mSelection;
}

/*public static void main(String[] args) {
	String [] file = {"Brandon" , "Sally"};
	StartChatInvitation SC = new StartChatInvitation("hello", file);
	System.out.println(SC.select());
}  */ 

private class SharedListSelectionHandler implements ListSelectionListener { //private class for list listener 
	public void valueChanged(ListSelectionEvent e) {
	    if (e.getValueIsAdjusting() == false) {

	        if (userlist.getSelectedIndex() == -1) {
               System.out.println("Nothing is selected");
	        } else {
	           System.out.println("In our chooser: " + userlist.getSelectedValue());
	           mSelection = (String)userlist.getSelectedValue();
	        }
	    }
	}
	}

public String selectValue(){
	return mSelection;
}
}
/*public class StartChatInvitation extends JDialog{
	
	private static final long serialVersionUID = 1L;
	private String [] Allname; //get those the list of allusername from ChatThread
	private JList userlist;
       public StartChatInvitation(String [] Allname){
    	super();
    	this.Allname = Allname;
   		createGUI();
   		addActions();
   		setResizable(false);
   		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
       }

public void createGUI(){
	 userlist = new JList(Allname); //data has type Object[]
	 userlist.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
	 userlist.setLayoutOrientation(JList.HORIZONTAL_WRAP);
	 userlist.setVisibleRowCount(-1);
	  
	 JScrollPane listScroller = new JScrollPane(Haslist);
	 listScroller.setPreferredSize(new Dimension(250, 100));
	   
	 ListSelectionModel listSelectionModel = Haslist.getSelectionModel();
	 listSelectionModel.addListSelectionListener(
	                            new SharedListSelectionHandler());
    }

public void addAction(){
	
}

}*/

