package client;

import java.io.Serializable;

public class UpdateFavoritesMessage implements Serializable {
	
	/*** default */
	private static final long serialVersionUID = 1L;
	private String username;
	private boolean adding;
	
	public UpdateFavoritesMessage(String user, boolean addOrNot)
	{
		username = user;
		adding = addOrNot;
	}
	
	//returns whether the account is to be added to favorites or not (if not, then it's to be deleted
	public boolean toAdd()
	{
		return adding;
	}
	
	public String getUsername() { return username; }

}
