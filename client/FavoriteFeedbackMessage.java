package client;

import java.io.Serializable;

public class FavoriteFeedbackMessage  implements Serializable{


	private static final long serialVersionUID = 1L;
	private String feedback;
	public FavoriteFeedbackMessage(String feedback){
		this.feedback = feedback;
	}
	public String getFeedback() {
		return feedback;
	}
	public void setFeedback(String feedback) {
		this.feedback = feedback;
	}
}
