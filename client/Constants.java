package client;

//package name

public class Constants {

    //filepaths of icons
    public static String PROFILEICON = "";
    public static String HOMEICON = "";
    public static String CHATICON = "";
    public static String FAVORITESICON = "";
    
    //Feed constants

    //public static int FEED_PANEL_WIDTH = 500;
    //public static int FEED_PANEL_HEIGHT = 500;
    public static int FEED_PANEL_WIDTH = 935;
    public static int FEED_PANEL_HEIGHT = 565;
    public static String[] MATERIALS = new String[] {"Wood", "Metals/Minerals", "Clothing", "Furniture", "Books", "Food", "Medicine", "Money", "Volunteer/Time", "Other"};
    public static boolean DEBUGGING_FEED = false;
    
    // Client and Server Configure
    public static final String CLIENT_CONFIG_PATH = "src/resources/config/Client.config";
    public static final String SERVER_CONFIG_PATH = "src/resources/config/Server.config";
    public static final String SERVER_IP_TAG = "ServerIP:";
    public static final String PORT_TAG = "Port:";


    //method to call in the place of an implemented method [placeholder message]
    public static void printNotYetImplementedMessage(String methodName){
        System.out.println(methodName + " is not yet implemented.");
    }
    
}