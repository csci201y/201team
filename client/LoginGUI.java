package client;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import message.LoginMessage;

public class LoginGUI extends JDialog {
	private static final long serialVersionUID = 1;
	
	private JPanel loginPanel;
	private JLabel logoLabel;
	private JLabel usernameLabel;
	private JTextField usernameTextField;
	private JLabel passwordLabel;
	private JPasswordField passwordField;
	private JButton cancelButton;
	private JButton loginButton;
	
	private ProgramClientListener pcl;
	
	public LoginGUI(ProgramClientListener pcl) {
		super();
		this.pcl = pcl;
		instantiateComponents();
		createGUI();
		addActions();
		setModal(true);

		setVisible(false);

	}
	
	private void instantiateComponents() {
		loginPanel = new JPanel(new GridBagLayout());
		ImageIcon logoIcon = new ImageIcon("src/resources/img/logo.png");
		logoLabel = new JLabel(logoIcon);
		usernameLabel = new JLabel("  Username");
		usernameTextField = new JTextField(20);
		passwordLabel = new JLabel("  Password");
		passwordField = new JPasswordField(20);
		cancelButton = new JButton("Cancel");
		loginButton = new JButton("Login");
	}
	
	private void createGUI() {
		setSize(400, 200);
		setLocation(400, 100);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setTitle("Login");
		setLayout(new BorderLayout());
		
		JPanel imagePanel = new JPanel();
		imagePanel.setLayout(new BoxLayout(imagePanel, BoxLayout.X_AXIS));
		imagePanel.add(Box.createRigidArea(new Dimension(150,0)));
		imagePanel.add(logoLabel);
		imagePanel.add(Box.createRigidArea(new Dimension(150,0)));
		
		GridBagConstraints gbc = new GridBagConstraints();
		
		gbc.gridx = 3;
		gbc.gridy = 0;
		gbc.gridwidth = 1;
		loginPanel.add(imagePanel, gbc);
		
		gbc.gridx = 0;
		gbc.gridy = 1;
		gbc.gridwidth = 1;
		gbc.insets = new Insets(0, 0, 6, 6);
		loginPanel.add(usernameLabel, gbc);
		
		gbc.gridx = 1;
		gbc.gridy = 1;
		gbc.gridwidth = 7;
		loginPanel.add(usernameTextField, gbc);
		
		gbc.gridx = 0;
		gbc.gridy = 2;
		gbc.gridwidth = 1;
		loginPanel.add(passwordLabel, gbc);
		
		gbc.gridx = 1;
		gbc.gridy = 2;
		gbc.gridwidth = 7;
		loginPanel.add(passwordField, gbc);
		
		gbc.gridx = 2;
		gbc.gridy = 3;
		gbc.gridwidth = 1;
		gbc.insets = new Insets(2, 14, 16, 5);
		loginPanel.add(loginButton, gbc);
		
		gbc.gridx = 5;
		gbc.gridy = 3;
		gbc.gridwidth = 1;
		loginPanel.add(cancelButton, gbc);
		
		add(imagePanel, BorderLayout.NORTH);
		add(loginPanel, BorderLayout.CENTER);
		
		pack();
	}
	
	private void addActions() {
		loginButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				String username = usernameTextField.getText();
				String password = new String(passwordField.getPassword());
				String encryptedPassword = encrypt(password);
				LoginMessage lm = new LoginMessage(username, encryptedPassword);
				pcl.sendMessage(lm);
			}
		});
		cancelButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				dispose();
			}
		});
	}
	
	private String encrypt(String password) {
        String generatedPassword = null;
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(password.getBytes());
            byte[] bytes = md.digest();
            StringBuilder sb = new StringBuilder();
            for(int i = 0; i < bytes.length; ++i) {
                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
            }
            generatedPassword = sb.toString();
        } catch (NoSuchAlgorithmException nsae) {
            System.out.println("nsae: " + nsae.getMessage());
        }
        return generatedPassword;
	}
	
}
