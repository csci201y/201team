package client;

import java.awt.BorderLayout;
import java.io.EOFException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.SocketException;

import javax.swing.JOptionPane;

import message.DefaultFeedMessage;
import message.EmptyChatMessage;
import message.FavoriteFeedMessage;
import message.SearchMessage;
import message.SendFavoritesMessage;
import message.UpdateUserFeedMessage;
import message.ValidateMessage;
import server.ServerConstants;

// Read data coming from the server
public class ProgramClientListener extends Thread {
	private MainGUI mgui;
	private ObjectOutputStream oos;
	private ObjectInputStream ois;
	private Account c; //this is an account that gets returned for search
	
	private EmptyChatMessage ecm;

	public ProgramClientListener(MainGUI mgui, Socket socket) {
		this.mgui = mgui;
		try {
			this.oos = new ObjectOutputStream(socket.getOutputStream());
			this.ois = new ObjectInputStream(socket.getInputStream());
			this.start();
		} catch (IOException ioe) {
			System.out.println("ProgamClientListener Exception: " + ioe.getMessage());
		}
	}

	public void sendMessage(Object msg) {
		try {
			System.out.println((msg instanceof UpdateFavoritesMessage) + " for favorites!!!!");
			oos.writeObject(msg);
			oos.flush();
		} catch (IOException ioe) {
			System.out.println("sendMessage Exception: " + ioe.getMessage());
		}
	}
	
	public Account getAccount()
	{
		return c;
	}
	
	public EmptyChatMessage getECM(){
		return ecm;
	}

	public void run() {
		// Send the log-in information only once to the server
		// Continuously listen to the server for message

		try {
			while (true) {
				Object vm = ois.readObject();
				if (vm instanceof ValidateMessage) {
					if (((ValidateMessage) vm).isExists().equals(ServerConstants.LOGINFAILURE)) {
						JOptionPane.showMessageDialog(null, "Account Login Failure", "Login Error",
								JOptionPane.ERROR_MESSAGE);
					}
					else if (((ValidateMessage) vm).isExists().equals(ServerConstants.LOGINFAILURE_NOTEXIST)) {
						JOptionPane.showMessageDialog(null, "Non-existent Account", "Login Error",
								JOptionPane.ERROR_MESSAGE);
					}
					else if (((ValidateMessage) vm).isExists().equals(ServerConstants.LOGINSUCCESS)) {
						//Set account of MainGUI
						Account account = ((ValidateMessage) vm).getAccount();
						mgui.setAccount(account);
						System.out.println("mgui account desc: " + mgui.getAccount().getDescription());
						//Switch out top bar panel
						mgui.remove(mgui.getNotLoggedInTopBarPanel());
						mgui.makeTopBarPanel(account, this);
						mgui.add(mgui.getTopBarPanel(), BorderLayout.NORTH);
						//mgui.getTopBarPanel().setAccount(account);
						mgui.revalidate();
						mgui.repaint();
						mgui.getNotLoggedInTopBarPanel().getLoginGUI().dispose();
						//Set account of Feed
						mgui.getFeed().setAccount(account);
						mgui.getFeed().updateFeed();
					}
					else if (((ValidateMessage) vm).isExists().equals(ServerConstants.SIGNUPFAILURE_EXISTS)) {
						JOptionPane.showMessageDialog(null, "Username alreasy exists.", "Sign-Up Error",
								JOptionPane.ERROR_MESSAGE);
					}
					else if (((ValidateMessage) vm).isExists().equals(ServerConstants.SIGNUPSUCCESS)) {
						//Set account of MainGUI
						Account account = ((ValidateMessage) vm).getAccount();
						mgui.setAccount(account);
						//Switch out top bar panel
						mgui.remove(mgui.getNotLoggedInTopBarPanel());
						mgui.makeTopBarPanel(account, this);
						mgui.add(mgui.getTopBarPanel(), BorderLayout.NORTH);
						//mgui.getTopBarPanel().setAccount(account);
						mgui.revalidate();
						mgui.repaint();
						mgui.getNotLoggedInTopBarPanel().getSignUpGUI().dispose();
						//Set account of Feed
						if (mgui.getFeed() == null) {
							System.out.println("GOTEM");
						}
						mgui.getFeed().setAccount(account);
						mgui.getFeed().updateFeed();
					}
				} else if (vm instanceof UpdateUserFeedMessage) {
					if (Constants.DEBUGGING_FEED) System.out.println("we receieved the updateUserFeedMessage of " + 
								"\n\t messages " + ((UpdateUserFeedMessage)vm).getMessages().size() +
								"\n\t materials " + ((UpdateUserFeedMessage)vm).getMaterials().size() + 
								"\n\t amounts " + ((UpdateUserFeedMessage)vm).getAmounts().size());
					mgui.getFeed().updateInfoToDisplay((UpdateUserFeedMessage)vm);
					//mgui.getUserFeed().updateUserFeed((UpdateUserFeedMessage)vm);
				} else if (vm instanceof EmptyChatMessage) {
					this.ecm = (EmptyChatMessage) vm;
				} else if (vm instanceof FavoriteFeedMessage) {
					mgui.getAccount().updateFavoriteAccounts((FavoriteFeedMessage)vm);
				} else if (vm instanceof DefaultFeedMessage) {
					mgui.getAccount().updateDefaultFeedAccounts((DefaultFeedMessage)vm);
					if ( mgui.getFeed() != null) mgui.getFeed().updateDefaults((DefaultFeedMessage)vm);
				} else if (vm instanceof FavoriteFeedbackMessage) {
					if (((FavoriteFeedbackMessage) vm).getFeedback().equals(ServerConstants.ADD_FAV_FAILURE_ACCOUNT_NOTEXISTS)) {
						JOptionPane.showMessageDialog(null, "User does not exist.", "Update Favorites Error",
								JOptionPane.ERROR_MESSAGE);
					} else if (((FavoriteFeedbackMessage) vm).getFeedback().equals(ServerConstants.ADD_FAV_FAILURE_IN_FAV)) {
						JOptionPane.showMessageDialog(null, "User is already in favorites list.", "Update Favorites Error",
								JOptionPane.ERROR_MESSAGE);
					} else if (((FavoriteFeedbackMessage) vm).getFeedback().equals(ServerConstants.ADD_FAV_SUCCESS)) {
						System.out.println("Client: received ADD_FAV_SUCCESS message");
						mgui.getTopBarPanel().getFavoritesFrame().dispose();
						JOptionPane.showMessageDialog(null, "User successfully added.", "Update Favorites",
								JOptionPane.INFORMATION_MESSAGE);
					} else if (((FavoriteFeedbackMessage) vm).getFeedback().equals(ServerConstants.REMOVE_FAV_FAILURE_ACCOUNT_NOTEXISTS)) {
						JOptionPane.showMessageDialog(null, "User does not exist.", "Update Favorites Error",
								JOptionPane.ERROR_MESSAGE);
					} else if (((FavoriteFeedbackMessage) vm).getFeedback().equals(ServerConstants.REMOVE_FAV_FAILURE_NOT_IN_FAV)) {
						JOptionPane.showMessageDialog(null, "User is not in favorites list.", "Update Favorites Error",
								JOptionPane.ERROR_MESSAGE);
					} else if (((FavoriteFeedbackMessage) vm).getFeedback().equals(ServerConstants.REMOVE_FAV_SUCCESS)) {
						mgui.getTopBarPanel().getFavoritesFrame().dispose();
						JOptionPane.showMessageDialog(null, "User successfully removes.", "Update Favorites",
								JOptionPane.INFORMATION_MESSAGE);
					}
				} else if (vm instanceof SendFavoritesMessage) {
					mgui.getFeed().receiveFavorites((SendFavoritesMessage)vm);
				}
				else if(vm instanceof SearchMessage) {
					System.out.println("searched");
					mgui.switchToSearch(new SearchResultPane(((SearchMessage) vm).getAccount()));
				}
			}
		} catch (EOFException eofe) {
			System.out.println("End of File Exception----THIS IS NOT THE FAULT OF THE SERVER ***BY AN ANGRY SERENA***");
		} catch (SocketException se) {
			System.out.println("Server Disconnected!");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
