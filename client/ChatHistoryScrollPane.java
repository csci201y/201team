package client;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import message.ChatMessage;

public class ChatHistoryScrollPane extends JFrame implements Runnable {
	
	private ObjectInputStream ois;
	private ObjectOutputStream oos;
	
	private JScrollPane a1;
	private JScrollPane a2;
	private JLabel Connectuser;
	private JTextArea TextDisplay;
	private JTextArea TextEnter;
	private JButton SendText;
	
	private String caller;
	private String user;
	private ChatMessage cm;
	private ChatThread ct;
	public ChatHistoryScrollPane(String caller, String user, String Message, ChatThread ct){
		
		super();
        setTitle("Chatting~");
 		setMinimumSize(new Dimension(200,300));
 		setSize(300, 350);
		setLocationRelativeTo(null);
	    System.out.println("Hello Scrollpane");
	    
	    this.caller = caller;
	    this.user = user;
	    this.ct = ct;
	     
		 this.setLayout( new BorderLayout() );
		 JPanel temp = new JPanel();
    	 temp.setLayout(new BoxLayout(temp, BoxLayout.X_AXIS));
    	 
    	 ImageIcon chatImage = new ImageIcon("src/resources/img/chat.png");
 		 JLabel Chatinfo = new JLabel(chatImage);
 		 temp.add(Chatinfo);
    	 
		 Connectuser = new JLabel("Caller: " + caller);
		 temp.add(Connectuser);
		 add( temp, BorderLayout.NORTH );
		 
		 TextDisplay = new JTextArea(Message);
		 TextDisplay.setLineWrap(true);
		 a1 = new JScrollPane(TextDisplay);
		 add( a1, BorderLayout.CENTER );
		 TextDisplay.setEditable(false);
		 JPanel outer = new JPanel();
		 outer.setLayout(new BoxLayout(outer, BoxLayout.X_AXIS));
		 TextEnter = new JTextArea();
		 a2 = new JScrollPane(TextEnter);
		 SendText = new JButton("Send");
		 outer.add(a2);
		 outer.add(SendText);
		 add(outer, BorderLayout.SOUTH);
		 
		 doAction();
		 new Thread(this).start();
		 setVisible(true);
		 
	} 
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public void updateTextDisplay (String message){
		TextDisplay.setText(message);
	}
	/*public void updateTextDisplay (String message){
		TextDisplay.setText(message);
	}*/
	public void doAction(){
		SendText.addActionListener(new ActionListener() {

 			@SuppressWarnings("deprecation")
 			@Override
 			public void actionPerformed(ActionEvent e) {
 				 String mess = TextDisplay.getText();
 
 			     mess = mess + "\n " + user + ": " + TextEnter.getText(); //add the new send message		 				
 			     cm = new ChatMessage(user,caller, mess); //flush it once it updated 
 				 ct.sendChatMessage(cm);
 				 TextDisplay.setText(mess);
 				 TextEnter.setText("");
 				 System.out.println("update in ChatHistory");
 
 			     /*mess = mess+ "\n " + user + ": " + TextEnter.getText(); //add the new send message		 
 				 cm = new ChatMessage(user,caller, mess); //flush it once it updated 
 				 ct.sendChatMessage(cm);	
 				 TextDisplay.setText(mess);
 				 TextEnter.setText("");*/
 				 
 			}

 			});
  
	}
	
	public void setClean(){
		TextDisplay.setText("");
	}
	
	public String getCaller() {
		return caller;
	}
	public void setCaller(String caller) {
		this.caller = caller;
	}
	public JTextArea getTextDisplay(){
		return TextDisplay;
	}
	public void updateJLabel(String s){
		Connectuser.setText(s);
	}
	@Override
	public void run() {
		// TODO Auto-generated method stub
		
	}
	
/*	 public void run() {
	    	 while(true){
	    		 //cm = ct.getChatMessage();
	    		while(cm.getMessage() != null){	 //if new message 
	    		 if(ct.getChatMessage().getreceiveName() == user){ // if it's a message sent to the user 
	    		 String newmess = cm.getMessage(); //get new message and update 
	    		 TextEnter.setText(newmess);
	    		 }
	    		 }
	    	 }
	    } */
	    
    
}
