package client;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;

import message.UpdateFeedMessage;

public class FeedInfoItem<A extends Account> extends JPanel {

	/** auto-generated */
	private static final long serialVersionUID = 1L;
	public static final boolean DEBUGGING = Constants.DEBUGGING_FEED; //toggles debugging messages
	
	//data
	private A account;
	private String mostRecentAsk;
	private String material;
	private int amount;
	
	private boolean isOld; //for updating
	
	private String username; //of the account
		
	private JPanel panel1, panel2, panel3;
	private JLabel accountNameL, accountTypeL, profilePic, materialL, amountL;
	private JTextArea textArea;
	
	//*** constructor ***//
	// for testing only.
	public FeedInfoItem() {
		setUp();
	}
	
	public FeedInfoItem(A account) {
		//Constants.printNotYetImplementedMessage("Info()");
		this.account = account;
		
		setUp();
		addActionListeners();
		
		this.setVisible(true);
	}
	
	private void setUp() {
		this.setPreferredSize(new Dimension(600,100)); // 300
		
		this.setBackground(Color.gray);
		
		//preliminary stuff
		panel1 = new JPanel();
		panel1.setLayout(new BoxLayout(panel1, BoxLayout.LINE_AXIS));
		panel2 = new JPanel();
		panel2.setLayout(new BoxLayout(panel2, BoxLayout.LINE_AXIS));
		panel3 = new JPanel();
		panel3.setLayout(new BoxLayout(panel3, BoxLayout.PAGE_AXIS));
		panel3.setBackground(Color.white);
		this.setLayout(new BoxLayout(this, BoxLayout.LINE_AXIS));
		
		accountNameL = new JLabel("            ");
		accountTypeL = new JLabel();
		profilePic = new JLabel();
		materialL = new JLabel("             ");
		amountL = new JLabel("   ");
		textArea = new JTextArea();
		textArea.setText("");
		textArea.setEditable(false);
		
		Font font1 = new Font("Monospaced", Font.BOLD, 15);
		Font font2 = new Font("Serif", Font.PLAIN, 13);
		
		accountNameL.setFont(font1);
		materialL.setFont(font2);
		amountL.setFont(font2);
		
		panel1.add(Box.createHorizontalStrut(15));
		panel1.add(accountNameL);
		panel1.add(Box.createHorizontalGlue());
		panel1.add(accountTypeL);
		panel1.add(Box.createHorizontalStrut(10));
		panel2.add(Box.createHorizontalStrut(10));
		panel2.add(materialL);
		panel2.add(Box.createHorizontalStrut(10));
		panel2.add(amountL);
		panel2.add(Box.createHorizontalGlue());
		
		panel3.add(panel1);
		panel3.add(panel2);
		panel3.add(textArea);
		
		this.add(Box.createHorizontalStrut(10));
		this.add(profilePic);
		this.add(Box.createHorizontalStrut(10));
		this.add(panel3);
		this.add(Box.createHorizontalStrut(10));
	
		this.setVisible(true);
		isOld = true;
		
		if (DEBUGGING) {
			if (account instanceof CharityAccount) { this.setBackground(Color.red); }
			else if (account instanceof BusinessAccount) { this.setBackground(Color.green); }
			else if (account instanceof IndividualAccount) { this.setBackground(Color.yellow); }
			else { this.setBackground(Color.gray); }
		}
	}
	
	private void addActionListeners() {
		//add fancy actions here, if any
	}
	
	public UpdateFeedMessage updateMostRecentAsk() {
		return new UpdateFeedMessage(username);
	}
	
	public void reset() {
		accountNameL = new JLabel("            ");
		accountTypeL = new JLabel();
		profilePic = new JLabel();
		materialL = new JLabel("             ");
		amountL = new JLabel("   ");
		textArea = new JTextArea();
		textArea.setText("");
	}
	
	//getters and setters?
	public A getAccount() { return account; }
	public String getUsername() { return username; }
	public String getMostRecentAsk() { return mostRecentAsk; }
	public String getMaterial() { return material; }
	public int getAmount() { return amount; }
	public boolean isOld() { return isOld; }
	
	@SuppressWarnings("unchecked")
	public void setAccount(Account account) {
		this.account = (A) account;
		
		profilePic.setIcon(new ImageIcon(ProfilePanel.getImage(account.getProfilePicture())));
	}
	
	public void setUsername(String username) {
		this.username = username;
		accountNameL.setText(username);
	}
	public void setMessage(String message) {
		this.mostRecentAsk = message; 
		textArea.setText(message);
	}
	public void setMaterial(String material) {
		this.material = material; 
		materialL.setText("material: " + material);
	}
	public void setAmount(int amount) {
		this.amount = amount; 
		amountL.setText("amount: " + amount);
	}
	public void isOld(boolean bool) { isOld = bool; }
	
}
