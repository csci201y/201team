package client;

public class IndividualAccount extends Account {
	
	/*** default */
	private static final long serialVersionUID = 1L;

	public IndividualAccount(String user, String pass)
	{
		super(user, pass);
		super.setCanDonate(true);
		super.setCanChat(true);
		super.setCanReceiveDonations(false);
	}

}
