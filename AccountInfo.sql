DROP DATABASE IF EXISTS AccountInfo;
CREATE DATABASE AccountInfo;

USE AccountInfo;

DROP TABLE IF EXISTS AccountInfo;
CREATE TABLE AccountInfo(
	accountID INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    accountName VARCHAR(50) NOT NULL,
     passcode VARCHAR(50) NOT NULL,
	accountType VARCHAR(50) NOT NULL,
    profilePicNum INT(10) NOT NULL,
    contact VARCHAR(100) NOT NULL,
    description VARCHAR(500) NOT NULL
);

INSERT INTO AccountInfo (accountName, passcode,accountType,profilePicNum, contact, description) VALUES ('CS@SC', '5e52b4184b1022f4a1b99a27c8d0aa12', 'Charity', 9, 'cscamps@usc.edu', 'The CS@SC Summer Camps provide underrepresented K12 students with an 
opportunity to explore topics that are not typically covered in traditional curricula, specifically topics in compyer science 
including stand-alone programming, web-development, mobile app creation, and robotics.');#1Q
INSERT INTO AccountInfo (accountName, passcode,accountType,profilePicNum, contact, description) VALUES ('Sally', '5e52b4184b1022f4a1b99a27c8d0aa12', 'Individual', 5, 'sally@usc.edu', 'I love dogs!');#1Q
INSERT INTO AccountInfo (accountName, passcode,accountType,profilePicNum, contact, description)VALUES ('Best Buy','5e52b4184b1022f4a1b99a27c8d0aa12', 'Business', 8, 'office@bestbuy.com', 'American multinational consumer electronics corporation');#1Q
INSERT INTO AccountInfo (accountName, passcode,accountType,profilePicNum, contact, description) VALUES ('The Humane Society','5e52b4184b1022f4a1b99a27c8d0aa12', 'Charity', 5, 'donorcare@humanesociety.org', 'The Nation\'s largest and most effective animal protection organization.');#1Q

INSERT INTO AccountInfo (accountName, passcode,accountType,profilePicNum, contact, description) VALUES ('Doctors Without Borders','5e52b4184b1022f4a1b99a27c8d0aa12', 'Charity', 10, 'office@doctorswithoutborders.org', 'Voluntary medical care and advocacy in the United States and abroad.');#1Q
INSERT INTO AccountInfo (accountName, passcode,accountType,profilePicNum, contact, description) VALUES ('Emma','5e52b4184b1022f4a1b99a27c8d0aa12', 'Individual', 2, 'emma@usc.edu', 'Just a girl.');#1Q
INSERT INTO AccountInfo (accountName, passcode,accountType,profilePicNum, contact, description) VALUES ('Serena','5e52b4184b1022f4a1b99a27c8d0aa12', 'Individual', 6, 'serena@usc.edu', 'I love space!');#1Q

INSERT INTO AccountInfo (accountName, passcode,accountType,profilePicNum, contact, description) VALUES ('Duncan','5e52b4184b1022f4a1b99a27c8d0aa12', 'Individual', 3, 'duncan@usc.edu', 'Business guy');#1Q
INSERT INTO AccountInfo (accountName, passcode,accountType,profilePicNum, contact, description) VALUES ('Brandon','5e52b4184b1022f4a1b99a27c8d0aa12', 'Individual', 1, 'brandon@usc.edu', 'Just a guy');#1Q
INSERT INTO AccountInfo (accountName, passcode,accountType,profilePicNum, contact, description) VALUES ('Chris', '5e52b4184b1022f4a1b99a27c8d0aa12', 'Individual', 4, 'sally@usc.edu', 'Hah');#1Q

DROP TABLE IF EXISTS ChatInfo;
CREATE TABLE ChatInfo(
	accountID INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
	Sender VARCHAR(50) NOT NULL,
	Receiver VARCHAR(50) NOT NULL,
	Accepted INT(1) NOT NULL,
	Message VARCHAR(200) NOT NULL
    
);

DROP TABLE IF EXISTS ChatRequest;
CREATE TABLE ChatRequest(
	requestID INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
	Sender VARCHAR(50) NOT NULL,
	Receiver VARCHAR(50) NOT NULL
);

DROP TABLE IF EXISTS FavouriteInfo;
CREATE TABLE FavouriteInfo(
    favID INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    accountName VARCHAR(50) NOT NULL,
	favName VARCHAR(50) NOT NULL
);

INSERT INTO FavouriteInfo(accountName, favName) values('Brandon', 'CS@SC');
INSERT INTO FavouriteInfo(accountName, favName) values('CS@SC', 'Doctors Without Borders');
INSERT INTO FavouriteInfo(accountName, favName) values('Brandon', 'Duncan');
INSERT INTO FavouriteInfo(accountName, favName) values('CS@SC', 'Best Buy');
INSERT INTO FavouriteInfo(accountName, favName) values('Chris', 'Emma');
INSERT INTO FavouriteInfo(accountName, favName) values('Chris', 'Doctors Without Borders');
INSERT INTO FavouriteInfo(accountName, favName) values('Duncan', 'Sally');
INSERT INTO FavouriteInfo(accountName, favName) values('Best Buy', 'Emma');
INSERT INTO FavouriteInfo(accountName, favName) values('Sally', 'Serena');
INSERT INTO FavouriteInfo(accountName, favName) values('Sally', 'Best Buy');
INSERT INTO FavouriteInfo(accountName, favName) values('Emma', 'Brandon');
INSERT INTO FavouriteInfo(accountName, favName) values('The Humane Society', 'Chris');
INSERT INTO FavouriteInfo(accountName, favName) values('Emma', 'Chris');
INSERT INTO FavouriteInfo(accountName, favName) values('The Humane Society', 'Brandon');
INSERT INTO FavouriteInfo(accountName, favName) values('Serena', 'Duncan');
INSERT INTO FavouriteInfo(accountName, favName) values('Serena', 'Best Buy');
INSERT INTO FavouriteInfo(accountName, favName) values('Best Buy', 'The Humane Societya');
INSERT INTO FavouriteInfo(accountName, favName) values('Best Buy', 'CS@SC');


##DROP TABLE IF EXISTS FeedInfo;
CREATE TABLE FeedInfo(
    feedID INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    username VARCHAR(50) NOT NULL,
    message VARCHAR(100) NOT NULL,
	material VARCHAR(50) NOT NULL,
    quantity INT(11) NOT NULL
);

INSERT INTO FeedInfo(username, message, material, quantity) values ('Brandon', 'Oak wood is good.', 'wood', 5);
INSERT INTO FeedInfo(username, message, material, quantity) values ('Brandon', 'Looking for shoes.', 'clothing', 20);
INSERT INTO FeedInfo(username, message, material, quantity) values ('Brandon', 'Any kind of firewood.', 'wood', 15);
INSERT INTO FeedInfo(username, message, material, quantity) values ('Chris', 'Twigs only.', 'wood', 7);
INSERT INTO FeedInfo(username, message, material, quantity) values ('Chris', 'Iron iron.', 'iron', 11);
INSERT INTO FeedInfo(username, message, material, quantity) values ('Sally', 'Birch wood is great.', 'wood', 45);
INSERT INTO FeedInfo(username, message, material, quantity) values ('Sally', 'Sweatshirts only please.', 'clothing', 8);
INSERT INTO FeedInfo(username, message, material, quantity) values ('Duncan', 'CSCI books please', 'books', 10);
INSERT INTO FeedInfo(username, message, material, quantity) values ('Duncan', 'For burning', 'wood', 22);
INSERT INTO FeedInfo(username, message, material, quantity) values ('Emma', 'Alice\'s Adventures in Wonderland', 'books', 18);
INSERT INTO FeedInfo(username, message, material, quantity) values ('Emma', 'Sweets', 'food', 5);
INSERT INTO FeedInfo(username, message, material, quantity) values ('Serena', 'Red', 'clothing', 16);
INSERT INTO FeedInfo(username, message, material, quantity) values ('Serena', 'For burning', 'books', 18);
INSERT INTO FeedInfo(username, message, material, quantity) values ('Serena', 'Devastated for food.', 'food', 33);



INSERT INTO FeedInfo(username, message, material, quantity) values ('CS@SC', 'Need people with basic Java and Scratch experience', 'volunteer/time', 50);
INSERT INTO FeedInfo(username, message, material, quantity) values ('CS@SC', 'Technical books would be the best.', 'books', 13);

INSERT INTO FeedInfo(username, message, material, quantity) values ('Best Buy', 'Looking to sponsor a CS summer camp with 75 monitors', 'other', 75);
INSERT INTO FeedInfo(username, message, material, quantity) values ('Best Buy', 'Want to donate some technical books.', 'books', 50);

INSERT INTO FeedInfo(username, message, material, quantity) values ('The Humane Society', 'Need dog/cat food', 'food', 99);
INSERT INTO FeedInfo(username, message, material, quantity) values ('The Humane Society', 'Some cloth for animals.', 'clothing', 30);

INSERT INTO FeedInfo(username, message, material, quantity) values ('Doctors Without Borders', 'Need people with medical experience', 'volunteer/time', 20);
INSERT INTO FeedInfo(username, message, material, quantity) values ('Doctors Without Borders', 'As much medicine as possible.', 'medicine', 80);